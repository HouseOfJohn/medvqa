
import os
from abc import ABC, abstractmethod
from collections import OrderedDict
from copy import deepcopy

import matplotlib.pyplot as plt
from registrable import Registrable

from constants import categories_names
from src.Experiment import Experiment
from src.utils import get_data_dataframes

experiment = Experiment(name="QuestionsParser", export_output=True, deep_learning=False)

class QuestionsParser(ABC, Registrable):

    @abstractmethod
    def create_questions_groups(self, cat_df, plot=False):
        pass

    def plot_charts(self, questions, questions_dict, category):
        labels = list(questions_dict.keys())
        sizes = [len(v) for k, v in questions_dict.items()]
        sizes_unique = [len(set(v)) for k, v in questions_dict.items()]
        assert sum(sizes) == len(questions)
        assert sum(sizes_unique) == len(set(questions))
        self.plot_pie_chart(labels, sizes, title=f"{category} questions")
        self.plot_pie_chart(labels, sizes_unique, title=f"Unique {category} questions")

    def plot_pie_chart(self, labels, values, title):
        fig, ax = plt.subplots(figsize=(12, 8))
        ax.pie(values, labels=labels, autopct=make_autopct(values),
                shadow=True, startangle=90)
        ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        fig.suptitle(title)  # or plt.suptitle('Main title')
        if experiment.export_output:
            fig_p = os.path.join(experiment.output_dir, title + "_plot.png")
            plt.savefig(fig_p)
            print(f'saved fig to path: {fig_p}')
        # plt.show()

    def partition_questions(self, questions, question_type):
        question_type_questions = [q for q in questions if question_type in q or question_type in q.replace("\t", "")]
        curr_questions = [q for q in questions if q not in question_type_questions]
        # print(f"{question_type} questions {len(question_type_questions)}, {len(curr_questions)} remaining")
        return curr_questions, question_type_questions, set(question_type_questions)

    def rewrite_questions(self, df, simplify_question):
        self.already_printed = set()
        df['question'] = df['question'].apply(lambda question: self.rewrite_single_question(question, simplify_question=simplify_question))
        return df

    def rewrite_single_question(self, question, simplify_question=True):
        question_group = self.get_question_group(question)
        if '[arg]' in question_group or '[arg1]' in question_group:
            question_rewrite, args = self.assign_args(question_group, question)
        else:
            question_rewrite = question_group
            args = None
        if simplify_question:
            simplified_question = self.simplify_question_rewrite(question, question_group, question_rewrite, args)
            question_rewrite = simplified_question
        return question_rewrite


    def get_question_group(self, question):
        for (question_group, group_set) in self.questions_sets:
            if question in group_set:
                return question_group
        raise Exception(f"Group not found {question}")

@QuestionsParser.register("abnormality_parser")
class AbnormalitiesParser(QuestionsParser):
    def __init__(self):
        super().__init__()
        self.general_abnormality_questions = []


    def create_questions_groups(self, cat_df, plot=False):
        questions = list(cat_df['question'].values)
        print(f"Abnormality, total of {len(questions)} questions")
        curr_questions = deepcopy(questions)

        are_there_abnormalities_in_this_arg_questions = [q for q in curr_questions if
                                                         "are there abnormalities in this" in q or
                                                         "is there an abnormality in" in q]
        curr_questions = [q for q in curr_questions if q not in are_there_abnormalities_in_this_arg_questions]

        does_this_image_look_normal_questions = [q for q in curr_questions if "does this image look normal" in q or "is this image normal" in q]
        curr_questions = [q for q in curr_questions if q not in does_this_image_look_normal_questions]

        is_the_arg_normal = [q for q in curr_questions if q.startswith("is the ") or q.startswith("is this a normal")]
        curr_questions = [q for q in curr_questions if q not in is_the_arg_normal]

        is_there_evidance_of_abnormalities = [q for q in curr_questions if q.startswith("is there evidence") or q.startswith("is there something wrong") or "is there an abnormality in the image" in q]
        curr_questions = [q for q in curr_questions if q not in is_there_evidance_of_abnormalities]

        what_abnormality_is_seen = [q for q in curr_questions if q.startswith("what abnormality is seen in the image") or "what is the primary abnormality in this image" in q]
        curr_questions = [q for q in curr_questions if q not in what_abnormality_is_seen]

        what_is_abnormal_in_the = [q for q in curr_questions if q.startswith("what is abnormal in the") or "what is most alarming" in q ]
        curr_questions = [q for q in curr_questions if q not in what_is_abnormal_in_the]
        assert len(curr_questions) == 0  # finished clustering all questions

        questions_dict = OrderedDict([('Are there abnormalities in this [arg]', are_there_abnormalities_in_this_arg_questions),
                                      ('Does this image look normal', does_this_image_look_normal_questions),
                                      ('Is this a normal [arg]', is_the_arg_normal),
                                      ('Is there abnormality in the image', is_there_evidance_of_abnormalities),
                                      ('What abnormality is seen', what_abnormality_is_seen),
                                      ('What is abnormal about this [arg]', what_is_abnormal_in_the)])

        if plot:
            self.plot_charts(questions, questions_dict, 'Abnormality')

        self.questions_sets = [(t[0], set(t[1])) for t in questions_dict.items()]
        print("Done parsing - Abnormality")

    def simplify_question_rewrite(self, question, question_group, question_rewrite, args):
        simplified_question = None
        if question_group == "Are there abnormalities in this [arg]":
            simplified_question = "abnormalities " + args[0]
        elif question_group == "Does this image look normal":
            simplified_question = "normal"
        elif question_group == "Is this a normal [arg]":
            simplified_question = "normal " + args[0]
        elif question_group == "Is there abnormality in the image":
            simplified_question = "abnormality"
        elif question_group == "What abnormality is seen":
            simplified_question = "abnormality seen"
        elif question_group == "What is abnormal about this [arg]":
            simplified_question = "abnormal" + args[0]
        else:
            raise Exception("Unknown question group")
        return simplified_question

    def assign_args(self, question_group, question, verbose=False):
        question_group = question_group.replace("?", "")
        question = question.replace("?", "")
        question_words = question.split(" ")

        start_loc = -1
        for start_w in ['this', 'the', 'normal']:
            if start_w in question_words:
                start_loc = question_words.index(start_w)
                break

        end_loc = -1
        for end_w in ['normal']:
            if end_w in question_words and question_words.index(end_w) > start_loc:
                end_loc = question_words.index(end_w)
                break

        if end_loc != -1:
            arg = " ".join(question_words[start_loc + 1: end_loc])
        else:
            arg = " ".join(question_words[start_loc + 1:])
        if arg == 'a':
            if question.startswith("is this a normal"):
                arg = " ".join(question_words[4:])
        question_group = question_group.replace("[arg]", arg)
        if verbose and question not in self.already_printed:
            print(f"Question: {question}, arg: {arg}")
            print(f"Rewrite: {question_group}\n")
            self.already_printed = self.already_printed.union([question])
        return question_group, [arg]


@QuestionsParser.register("organ_parser")
class OrganParser(QuestionsParser):
    def __init__(self):
        super().__init__()
        self.general_organ_questions = ['the image shows what organ system?',
                                        'what is one organ system seen in this image?',
                                        'what is the organ principally shown in this image?',
                                        'what is the organ system in this image?',
                                        'what organ is this image of?', 'what organ system is being imaged?',
                                        'what organ system is evaluated primarily?', 'what organ system is imaged?',
                                        'what organ system is pictured here?',
                                        'what organ system is primarily present in this image?',
                                        'what organ system is shown in the image?',
                                        'what organ system is shown in this image?', 'what organ system is visualized?',
                                        'what organ systems can be evaluated with this image?',
                                        'what part of the body does this image show?',
                                        'what part of the body is being imaged here?',
                                        'what part of the body is being imaged?',
                                        'which organ is captured by this image?',
                                        'which organ system is imaged?', 'which organ system is shown in the image?']

    def create_questions_groups(self, cat_df, plot=False):
        questions = list(cat_df['question'].values)
        print(f"Organ, total of {len(questions)} questions")
        curr_questions = deepcopy(questions)

        what_is_the_organ_in_this_image = [c for c in curr_questions if c in self.general_organ_questions]
        curr_questions = [c for c in curr_questions if c not in what_is_the_organ_in_this_image]
        what_organ_is_this_arg_showing = curr_questions


        questions_dict = OrderedDict([("what organ is this", what_is_the_organ_in_this_image),
                                       ("what organ is this [arg] showing", what_organ_is_this_arg_showing)])

        if plot:
            self.plot_charts(questions, questions_dict, 'Organ')

        self.questions_sets = [(t[0], set(t[1])) for t in questions_dict.items()]
        print("Done parsing - Organ")

    def simplify_question_rewrite(self, question, question_group, question_rewrite, args):
        simplified_question = None
        if question_group == "what organ is this":
            simplified_question = "organ"
        elif question_group == "what organ is this [arg] showing":
            simplified_question = "organ " + args[0]
        else:
            raise Exception("Unknown question group")
        return simplified_question

    def assign_args(self, question_group, question, verbose=False):
        question_group = question_group.replace("?", "")
        question = question.replace("?", "")
        question_words = question.split(" ")

        start_loc = -1
        for start_w in ['this', 'the']:
            if start_w in question_words:
                start_loc = question_words.index(start_w)
                break

        end_loc = -1
        for end_w in ['showing', 'show', 'shows']:
            if end_w in question_words and question_words.index(end_w) > start_loc:
                end_loc = question_words.index(end_w)
                break

        if end_loc != -1:
            arg = " ".join(question_words[start_loc + 1: end_loc])
        else:
            arg = " ".join(question_words[start_loc + 1: ])
        question_group = question_group.replace("[arg]", arg)
        if verbose and question not in self.already_printed:
            print(f"Question: {question}, arg: {arg}")
            print(f"Rewrite: {question_group}\n")
            self.already_printed = self.already_printed.union([question])
        return question_group, [arg]


@QuestionsParser.register("plane_parser")
class PlaneParser(QuestionsParser):
    def __init__(self):
        super().__init__()
        self.general_plane_questions = ['in what plane was this image taken?', 'what image plane is this?',
                                             'what imaging plane is depicted here?', 'what is the plane of the image?',
                                             'in what plane is this image oriented?', 'in what plane is this image taken?',
                                             'what is the plane?', 'what plane is demonstrated?', 'what plane is seen?',
                                             'what plane is the image acquired in?', 'what plane is this film',
                                        'what plane is this?', 'what plane was used?',
                                             'which plane is the image shown in?', 'which plane is the image taken?',
                                             'which plane is this image in?', 'which plane is this image taken?'
                                        ]

    def create_questions_groups(self, cat_df, plot=False):
        questions = list(cat_df['question'].values)
        print(f"Plane, total of {len(questions)} questions")
        curr_questions = deepcopy(questions)

        what_plane_is_this_questions = [c for c in curr_questions if c in self.general_plane_questions]
        curr_questions = [c for c in curr_questions if c not in what_plane_is_this_questions]
        what_plane_it_this_arg_questions = curr_questions

        questions_dict = OrderedDict([("what plane is this", what_plane_is_this_questions),
                                       ("what plane is this [arg] taken in", what_plane_it_this_arg_questions)])

        if plot:
            self.plot_charts(questions, questions_dict, 'Plane')

        self.questions_sets = [(t[0], set(t[1])) for t in questions_dict.items()]
        print("Done parsing - Plane")

    def simplify_question_rewrite(self, question, question_group, question_rewrite, args):
        simplified_question = None
        if question_group == "what plane is this":
            simplified_question = "plane"
        elif question_group == "what plane is this [arg] taken in":
            simplified_question = "plane " + args[0]
        else:
            raise Exception("Unknown question group")
        return simplified_question


    def assign_args(self, question_group, question, verbose=False):
        question_group = question_group.replace("?", "")
        question = question.replace("?", "")
        question_words = question.split(" ")

        start_loc = -1
        for start_w in ['this', 'the']:
            if start_w in question_words:
                start_loc = question_words.index(start_w)
                break

        end_loc = -1
        for end_w in ['taken', 'in', 'captured', 'displayed', 'image']:
            if end_w in question_words and question_words.index(end_w) > start_loc:
                end_loc = question_words.index(end_w)
                break

        if end_loc != -1:
            arg = " ".join(question_words[start_loc + 1: end_loc])
        else:
            arg = " ".join(question_words[start_loc + 1: ])
        if len(arg.split(" ")) >= 4:
            if question.startswith("what is the plane of the"):
                arg = " ".join(question_words[6: ])
        question_group = question_group.replace("[arg]", arg)
        if verbose and question not in self.already_printed:
            print(f"Question: {question}, arg: {arg}")
            print(f"Rewrite: {question_group}\n")
            self.already_printed = self.already_printed.union([question])
        return question_group, [arg]



@QuestionsParser.register("modality_parser")
class ModalityParser(QuestionsParser):
    def __init__(self):
        super().__init__()
        self.questions_with_modality = {'which image modality is this?', 'what is the modality?',
                'what type of imaging modality is used to acquire the image?', 'what imaging modality was used?',
                'what type of imaging modality is shown?', 'in what modality is this image taken?',
                'what modality is shown?', 'what imaging modality is used to acquire this picture?',
                'what modality is used to take this image?', 'what type of image modality is this?',
                'what type of imaging modality is seen in this image?', 'what is the modality of this image?',
                'what modality was used to take this image?', 'with what modality is this image taken?',
                'what imaging modality is seen here?', 'what type of image modality is seen?',
                'what imaging modality was used to take this image?', 'what imaging modality is used?'}
        self.questions_asking_what_kind_of_modality = {'what kind of image is this?', 'what was this image taken with?',
                                                       'what type of imaging does this represent?', 'how was the image taken?',
                                                       'how was this image taken?', 'what type of imaging is this?',
                                                       'what imaging method was used?', 'how is the image taken?',
                                                       'what kind of scan is this?', 'what type of imaging was used?'}
        self.what_is_the_modality_questions = self.questions_with_modality.union(self.questions_asking_what_kind_of_modality)

    def create_questions_groups(self, cat_df, plot=False):
        questions = list(cat_df['question'].values)
        print(f"Modality, total of {len(questions)} questions")
        curr_questions = deepcopy(questions)

        what_is_the_modality_questions = [q for q in curr_questions if q in self.what_is_the_modality_questions]
        curr_questions = [q for q in questions if q not in what_is_the_modality_questions]

        ### Sanity check
        df_with_modality_questions = cat_df[cat_df['question'].isin(self.questions_with_modality)]
        for q in self.questions_asking_what_kind_of_modality:
            # make sure that the answers for q are the same answer as the questions asking on modality
            q_df = cat_df[cat_df['question'] == q]
            assert q_df['answer'].apply(lambda x: x not in df_with_modality_questions['answer'].values).sum() == 0

        curr_questions, is_this_questions, is_this_questions_set = self.partition_questions(curr_questions, "is this")

        curr_questions, what_type_of_contrast_questions, what_type_of_contrast_set = self.partition_questions(curr_questions, "what type of contrast")

        curr_questions, was_specific_atom_contrast_given, was_specific_atom_contrast_given_set = self.partition_questions(curr_questions, "contrast given to the patient")

        curr_questions, was_specific_atom_taken_with_contrast, was_specific_atom_taken_with_contrast_set = self.partition_questions(curr_questions, "taken with contrast")

        curr_questions, what_is_the_mr_weighting, what_is_the_mr_weighting_set = self.partition_questions(curr_questions, "what is the mr weighting")

        assert len(curr_questions) == 0  # finished clustering all questions
        questions_dict = OrderedDict([("what is the modality", what_is_the_modality_questions),
                                       ("is this [arg1] /or [arg2] /or [arg3]", is_this_questions),
                                       ("what type of contrast", what_type_of_contrast_questions),
                                       ("was [arg] contrast given", was_specific_atom_contrast_given),
                                       ("was the [arg] taken with contrast", was_specific_atom_taken_with_contrast),
                                       ("what is the mr weighting", what_is_the_mr_weighting)])

        if plot:
            self.plot_charts(questions, questions_dict, 'Modaility')

        self.questions_sets = [(t[0], set(t[1])) for t in questions_dict.items()]

        print("Done parsing - Modality")

    def simplify_question_rewrite(self, question, question_group, question_rewrite, args):
        simplified_question = None
        if question_group == "what is the modality":
            simplified_question = "modality"
        elif question_group == "is this [arg1] /or [arg2] /or [arg3]":
            simplified_question = "is " + " or ".join(args)
        elif question_group == "what type of contrast":
            simplified_question = "contrast"
        elif question_group == "was [arg] contrast given":
            simplified_question = "was " + args[0] + " contrast"
        elif question_group == "was the [arg] taken with contrast":
            simplified_question = "was " + args[0] + " taken contrast"
        elif question_group == "what is the mr weighting":
            simplified_question = "weighting"
        else:
            raise Exception("Unknown question group")
        return simplified_question


    def assign_args(self, question_group, question, verbose=False):
        question_group = question_group.replace("?","")
        question = question.replace("?", "")
        question_words = question.split(" ")
        question_group_words = question_group.split(" ")
        in_question_but_not_in_question_group = [x for x in question_words[:len(question_group_words)] if x not in question_group_words]
        if '[arg]' in question_group:
            arg = " ".join(in_question_but_not_in_question_group).strip()
            question_group = question_group.replace("[arg]", arg)
            args = [arg]
        elif '[arg1]' in question_group:
            args, question_group = self.handle_arg1_question(question, question_group, question_words)
        else:
            raise Exception(f"Unrecognised arg: {question, question_group}")

        if verbose and question not in self.already_printed:
            # print(f"for question: {question}, arg is : {arg}, after assign: {question_group}")
            print(f"Question: {question}, args: {args}")
            print(f"Rewrite: {question_group}\n")
            self.already_printed = self.already_printed.union([question])
        return question_group, args

    def handle_arg1_question(self, question, question_group, question_words):
        if 'a' in question_words:
            a_location = question_words.index('a')
        elif 'an' in question_words:
            a_location = question_words.index('an')
        else:
            if question == 'is this image modality t1, t2, or flair':
                args = ['t1', 't2', 'flair']
                question_group = 'is this t1 /or t2 /or flair'
                return args, question_group
            else:
                raise Exception(f"Unknown question {question}")
        if 'or' in question:
            question_group, args = self.handle_or_question(a_location, question_group, question_words)
        else:
            arg = " ".join(question_words[a_location + 1:])
            question_group = " ".join(question_group.replace("[arg1]", arg).split(" ")[:len(question_words) - 1])
            args = [arg]
        return args, question_group

    def handle_or_question(self, a_location, question_group, question_words):
        or_locations = []
        for w_idx, w in enumerate(question_words):
            if w == 'or':
                or_locations.append(w_idx)
        first_or_location = or_locations[0]
        first_arg = " ".join(question_words[a_location + 1: first_or_location])
        args = [first_arg]
        question_group = " ".join(question_group.replace("[arg1]", first_arg).split(" ")[:len(question_words) - 1])
        if len(or_locations) == 1:
            second_arg = " ".join(question_words[first_or_location + 1:])
            args.append(second_arg)
            question_group = " ".join(question_group.replace("[arg2]", second_arg).split(" ")[:len(question_words) - 1])
        elif len(or_locations) == 2:
            second_or_location = or_locations[1]
            second_arg = " ".join(question_words[first_or_location + 1:second_or_location])
            third_arg = " ".join(question_words[second_or_location + 1:])
            args.append(second_arg)
            args.append(third_arg)
            question_group = " ".join(question_group.replace("[arg2]", second_arg).split(" ")[:len(question_words) - 1])
            question_group = " ".join(question_group.replace("[arg3]", third_arg).split(" ")[:len(question_words) - 1])
        else:
            raise Exception(f"Too many ors")
        return question_group, args


def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        pct = int(pct)
        return '{p:.0f}%\n({v:d})'.format(p=pct,v=val)
    return my_autopct

def main():
    df_all, df_train, df_valid, df_test = get_data_dataframes()

    for category in categories_names:
    # category = 'abnormality' # ['modality', 'plane', 'organ', 'abnormality']
        cat_df = df_all[df_all['category'] == category]
        category_parser = QuestionsParser.by_name(f"{category}_parser")()
        category_parser.create_questions_groups(cat_df, plot=True)

    print("Done")

if __name__ == '__main__':
    main()