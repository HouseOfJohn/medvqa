import operator
import os
from collections import defaultdict
from copy import deepcopy

import matplotlib.pyplot as plt
import pandas as pd
from nltk.translate.bleu_score import corpus_bleu
from simpletransformers.classification import ClassificationModel
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import train_test_split

from src.Experiment import Experiment
from constants import dataset_type_to_dir_map, data_dir
from src.utils import fix_y_test

categories_names = ['abnormality']

data_dir_2020 = os.path.join(data_dir, 'ImageCLEF_data_2020')
train_dir_2020 = os.path.join(data_dir_2020, 'VQAMed2020-VQAnswering-TrainingSet')
valid_dir_2020 = os.path.join(data_dir_2020, 'VQAMed2020-VQAnswering-ValidationSet')
test_dir_2020 = os.path.join(data_dir_2020, 'VQA-Med-2020-Task1-VQAnswering-TestSet')
all_images_2019_path = os.path.join(data_dir_2020, 'all_2019_images')
all_images_2020_path = os.path.join(data_dir_2020, 'all_2020_images')
dataset_type_to_dir_map_2020 = {'train': train_dir_2020, 'valid': valid_dir_2020, 'test': test_dir_2020}


def main():

    datasets = get_data_dataframes_and_export_stats()

    all_2019_answers = set(datasets['2019']['all']['answer'])
    all_2020_answers = set(datasets['2020']['all']['answer'])

    correlation_2019_2020_answers = pd.Series({
        '2019 Answers': len(all_2019_answers),
        '2020 Answers': len(all_2020_answers),
        'Intersection 2019-2020': len(all_2020_answers.intersection(all_2019_answers)),
        'In 2020 but not in 2019': len(all_2020_answers.difference(all_2019_answers)),
    }, name='Correlation 2019 2020 answers')
    print(correlation_2019_2020_answers)
    correlation_2019_2020_answers.to_excel(os.path.join(experiment.output_dir, f"Correlation 2019 2020 answers.xlsx"))

    for k in datasets:

        print(f"Working on dataset: {k}\n")

        get_number_of_words_stats(datasets[k]['all'], datasets[k]['test'], datasets[k]['train'], datasets[k]['valid'], k)

        check_bert_vocab_intersection(datasets[k]['all'], k)

        produce_answers_distribution_plots(datasets[k]['all'], k)

        if k != '2020': # 2020 doesn't have answers for the test set

            predict_with_most_common_ans_per_category(datasets[k]['test'], k)

            in_test_but_not_in_train = check_correlation_with_test(datasets[k]['train'], datasets[k]['valid'], datasets[k]['test'], k)

            predict_with_most_common_ans_given_question(datasets[k]['all'], datasets[k]['test'], in_test_but_not_in_train, k)

        print(f"Finished with dataset: {k}\n")

    print("Done")


def get_number_of_words_stats(all_df, df_test, df_train, df_valid, dataset_name):
    num_words, num_distinct_words = get_words_number(all_df)
    num_words_train, num_distinct_words_train = get_words_number(df_train)
    num_words_valid, num_distinct_words_valid = get_words_number(df_valid)
    num_words_test, num_distinct_words_test = get_words_number(df_test)
    s_all = pd.Series({'Total': num_words, 'Distinct': num_distinct_words}, name='All')
    s_train = pd.Series({'Total': num_words_train, 'Distinct': num_distinct_words_train}, name='Train')
    s_valid = pd.Series({'Total': num_words_valid, 'Distinct': num_distinct_words_valid}, name='Valid')
    s_test = pd.Series({'Total': num_words_test, 'Distinct': num_distinct_words_test}, name='Test')
    number_of_words_stats = pd.DataFrame([s_all, s_train, s_valid, s_test])
    print("Number of words stats")
    print(number_of_words_stats)
    print()
    number_of_words_stats.to_excel(os.path.join(experiment.output_dir, f"Number of words stats - {dataset_name}.xlsx"))


def get_bert_word_pieces_difference(in_vocab_but_not_in_bert, bert_base_vocab):
    model = ClassificationModel('bert', 'bert-base-cased', use_cuda=False)
    word_pieces_not_in_bert_vocab = defaultdict(list)
    for w in in_vocab_but_not_in_bert:
        tokenized_w_parts = model.tokenizer.tokenize(w)
        for tokenized_part in tokenized_w_parts:
            if tokenized_part not in bert_base_vocab:
                word_pieces_not_in_bert_vocab[w].append(tokenized_part)
    return word_pieces_not_in_bert_vocab


def check_bert_vocab_intersection(all_df, dataset_name):
    all_question_words_lst, all_question_words_set = get_all_question_words(all_df)

    with open(data_dir + os.sep + "vocabs_and_utils" + os.sep + "bert_base_vocab.txt", encoding='utf-8') as f:
        bert_base_vocab = set([x.rstrip('\n') for x in f.readlines()]) # 30K words

    with open(data_dir + os.sep + "vocabs_and_utils" + os.sep + "biobert_vocab.txt", encoding='utf-8') as f:
        biobert_vocab = set([x.rstrip('\n') for x in f.readlines()]) # 28K words

    print("Correlation with BERT and BioBERT")
    in_vocab_but_not_in_bert_vocab = all_question_words_set.difference(bert_base_vocab)
    in_vocab_but_not_in_bio_bert_vocab = all_question_words_set.difference(biobert_vocab)

    in_vocab_but_not_in_bert = get_bert_word_pieces_difference(in_vocab_but_not_in_bert_vocab, bert_base_vocab)
    in_vocab_but_not_in_bio_bert = get_bert_word_pieces_difference(in_vocab_but_not_in_bio_bert_vocab, biobert_vocab)

    corr_with_bert_and_bio_bert = {'Bert word-piece VOCAB': len(bert_base_vocab),
                                   'BioBert word-piece VOCAB': len(biobert_vocab),
                                   'Word-pieces in vocab but not in Bert': len(in_vocab_but_not_in_bert),
                                   'Word-pieces in vocab but not in Bio-Bert': len(in_vocab_but_not_in_bio_bert),
                                   'BERT & BioBERT word piece intersection': len(bert_base_vocab.intersection(biobert_vocab)),
                                  }
    corr_with_bert_and_bio_bert_series = pd.Series(corr_with_bert_and_bio_bert)
    print(corr_with_bert_and_bio_bert_series)
    corr_with_bert_and_bio_bert_series.to_excel(os.path.join(experiment.output_dir, f"Correlation with BERT and BioBERT - {dataset_name}.xlsx"))
    if len(in_vocab_but_not_in_bert) > 0:
        print('Word-pieces in vocab but not in Bert')
        print(dict(in_vocab_but_not_in_bert))

    if len(in_vocab_but_not_in_bio_bert) > 0:
        print('Word-pieces in vocab but not in Bio-Bert')
        print(dict(in_vocab_but_not_in_bio_bert))

    print()


def get_words_number(df):
    ws1, ws2 = get_all_question_words(df)
    return len(ws1), len(ws2)

def get_all_question_words(df):
    all_sents = df['question'].values
    all_words = []
    for sent in all_sents:
        sent_words = sent.split(" ")
        if '?' in sent_words[-1]:
            sent_words[-1] = sent_words[-1].replace("?", "")
        all_words += sent_words
    return all_words, set(all_words)


def get_dict_most_common_ans_given_question(df):
    interesting_columns = ['question', 'answer']
    df = df[interesting_columns]
    most_common_ans_given_question = {}

    grouped = df.groupby('question')
    for question, group in grouped:
        val_counts = group['answer'].value_counts()
        most_common_ans_for_question = max(val_counts.items(), key=operator.itemgetter(1))[0]
        most_common_ans_given_question[question] = most_common_ans_for_question

    return most_common_ans_given_question


def get_instances_of_same_question_and_answer_and_qa_id(df, df_test):
    intersection_question_answer = pd.merge(df, df_test, how='inner', on=['question', 'answer', 'qa_id'])
    return len(intersection_question_answer)


def check_correlation_with_test(df_train, df_valid, df_test, dataset_name):
    print("Checking correlation of train with test")
    print()

    train_questions_lst = df_train['question'].values
    test_questions_lst = df_test['question'].values
    question_stats_lst = {'Train': len(train_questions_lst), 'Test': len(test_questions_lst),
                          'Intersection': len([x for x in train_questions_lst if x in test_questions_lst]),
                          'In train but not in test': len([x for x in train_questions_lst if x not in test_questions_lst]),
                          'In test but not in train': len([x for x in test_questions_lst if x not in train_questions_lst])}
    corr_series = pd.Series(question_stats_lst, name='Question stats')
    print(corr_series)
    print()

    corr_series.to_excel(os.path.join(experiment.output_dir, f"Correlation of train with test - {dataset_name}.xlsx"))


    train_questions_set, test_questions_set = set(train_questions_lst), set(test_questions_lst)
    in_test_but_not_in_train = test_questions_set.difference(train_questions_set)
    question_stats_set = {'Train': len(train_questions_set), 'test': len(test_questions_set),
                          'Intersection': len(train_questions_set.intersection(test_questions_set)),
                          'In train but not in test': len(train_questions_set.difference(test_questions_set)),
                          'In test but not in train': len(test_questions_set.difference(train_questions_set))}
    corr_unique_series = pd.Series(question_stats_set, name='Unique question stats stats')
    print(corr_unique_series)
    print()

    corr_series.to_excel(os.path.join(experiment.output_dir, f"Correlation of train with test - Unique - {dataset_name}.xlsx"))

    same_instances_train_test = get_instances_of_same_question_and_answer_and_qa_id(df_train, df_test)
    same_instances_valid_test = get_instances_of_same_question_and_answer_and_qa_id(df_valid, df_test)

    if same_instances_train_test > 0 or same_instances_valid_test > 0:
        print('*** Found overlaps with test')

    return in_test_but_not_in_train


def predict_with_most_common_ans_per_category(df_test, dataset_name):
    most_common_answer_for_each_cat = {'abnormality': 'yes'}

    df_test = df_test[df_test['category'].isin(categories_names)]
    df_test['most_common_ans'] = df_test['category'].apply(lambda cat: most_common_answer_for_each_cat[cat])

    stats_df = get_performance_stats(df_test)

    print(f"Predict with most common answer per category")
    print(stats_df)
    print()

    stats_df.to_excel(os.path.join(experiment.output_dir, f"Predict with most common answer per category - {dataset_name}.xlsx"))


def get_performance_stats(df_test):
    stats_df = pd.DataFrame()
    for category in categories_names:
        df_test_cat = df_test[df_test['category'] == category]
        y_true, y_pred = list(df_test_cat['answer'].values), list(df_test_cat['most_common_ans'].values)
        acc, f1_score_val, bleu_score = fix_y_test_and_get_metrics(y_pred, y_true)
        cat_series = pd.Series({'Accuracy': acc, 'F1-Score': f1_score_val, 'BLEU': bleu_score}, name=category)
        stats_df = stats_df.append(cat_series)
    stats_df['Accuracy'] = stats_df['Accuracy'].apply(lambda x: round(x, 3))
    stats_df['BLEU'] = stats_df['BLEU'].apply(lambda x: round(x, 3))
    mean = stats_df.mean().apply(lambda x: round(x, 3))
    stats_df = stats_df.append(pd.Series(mean, name='Average'))
    return stats_df

def get_performance_most_common_ans_per_question_and_category(df_train_val, df_test):
    stats_df = pd.DataFrame()
    for category in categories_names:
        df_test_cat = df_test[df_test['category'] == category]
        df_train_val_cat = df_train_val[df_train_val['category'] == category]
        most_common_ans_given_question_per_cat = get_dict_most_common_ans_given_question(df_train_val_cat)
        df_test_cat['most_common_ans'] = df_test_cat['question'].apply(lambda q: most_common_ans_given_question_per_cat[q])
        y_true, y_pred = list(df_test_cat['answer'].values), list(df_test_cat['most_common_ans'].values)
        acc, f1_score_val, bleu_score = fix_y_test_and_get_metrics(y_pred, y_true)
        cat_series = pd.Series({'Accuracy': acc, 'F1-Score': f1_score_val, 'BLEU': bleu_score}, name=category)
        stats_df = stats_df.append(cat_series)
    stats_df['Accuracy'] = stats_df['Accuracy'].apply(lambda x: round(x, 3))
    stats_df['BLEU'] = stats_df['BLEU'].apply(lambda x: round(x, 3))
    mean = stats_df.mean().apply(lambda x: round(x, 3))
    mean_without_abnormality = stats_df.drop(['abnormality']).mean().apply(lambda x: round(x, 3))
    stats_df = stats_df.append(pd.Series(mean, name='Average'))
    stats_df = stats_df.append(pd.Series(mean_without_abnormality, name='Average without abnormality'))
    return stats_df


def predict_with_most_common_ans_given_question(all_df, df_test, in_test_but_not_in_train, dataset_name):
    train_val_df = all_df[all_df['dataset_type'].isin(['train', 'val'])]
    most_common_ans_given_question = get_dict_most_common_ans_given_question(train_val_df)

    df_test = df_test[df_test['question'].apply(lambda x: x not in in_test_but_not_in_train)]
    df_test['most_common_ans'] = df_test['question'].apply(lambda q: most_common_ans_given_question[q])

    stats_df_per_question = get_performance_stats(df_test)
    print(f"Predict with most common answer per question")
    print(stats_df_per_question)
    print()
    stats_df_per_question.to_excel(os.path.join(experiment.output_dir, f"Predict with most common answer per question - {dataset_name}.xlsx"))

    test_most_common_ans_given_question = get_dict_most_common_ans_given_question(df_test)
    df_test['most_common_ans'] = df_test['question'].apply(lambda q: test_most_common_ans_given_question[q])
    stats_df_per_question = get_performance_stats(df_test)
    print(f"Predict with most common answer in test per question")
    print(stats_df_per_question)
    print()

    stats_df_per_question.to_excel(os.path.join(experiment.output_dir, f"Predict with most common answer in test per question - {dataset_name}.xlsx"))

    print()

def fix_y_test_and_get_metrics(y_pred, y_true):
    y_test_fixed = fix_y_test(y_true, y_pred)
    acc = round(accuracy_score(y_test_fixed, y_pred), 2)
    f1_score_val = round(f1_score(y_test_fixed, y_pred, average='weighted'), 2)
    wrapped_y_test_fixed = [[x] for x in y_test_fixed]
    bleu_score = corpus_bleu(wrapped_y_test_fixed, y_pred)
    rounded_bleu_score = round(bleu_score, 2)
    return acc, f1_score_val, rounded_bleu_score


def get_data_dataframes_and_export_stats():
    datasets = get_datasets()

    datasets_stats_df = get_datasets_stats_df(datasets)

    print("Datasets stats")
    print(datasets_stats_df)
    print()
    datasets_stats_df.to_excel(os.path.join(experiment.output_dir, "Datasets stats.xlsx"))

    for k in datasets:
        for dataset_type in ['train', 'valid', 'test']:
            ds = datasets[k][dataset_type]['dataset_type']
            assert len(set(ds)) == 1 and list(ds)[0] == dataset_type

    return datasets


def get_datasets():
    all_df_20, df_test_20, df_train_20, df_valid_20 = get_2020_data()
    all_df_19, df_test_19, df_train_19, df_valid_19 = get_2019_data()
    df_train_train_20, df_train_val_20 = train_test_split(df_train_20, test_size=0.25)
    trainable_2020_ds = get_trainable_2020_data(deepcopy(df_train_train_20), deepcopy(df_train_val_20),
                                                deepcopy(df_valid_20))
    merged_2019_2020_ds = get_merged_19_20_data(deepcopy(all_df_19), deepcopy(df_train_train_20),
                                                deepcopy(df_train_val_20), deepcopy(df_valid_20))
    merged_2019_2020_same_ans_ds = get_merged_19_20_data_same_ans(deepcopy(all_df_19), deepcopy(df_train_train_20),
                                                                  deepcopy(df_train_val_20), deepcopy(df_valid_20),
                                                                  deepcopy(all_df_20))
    datasets = {'2019': {'all': all_df_19, 'test': df_test_19, 'train': df_train_19, 'valid': df_valid_19},
                '2020': {'all': all_df_20, 'test': df_test_20, 'train': df_train_20, 'valid': df_valid_20},
                'trainable_2020': trainable_2020_ds,
                'merged_2019_2020': merged_2019_2020_ds,
                'merged_2019_2020_same_ans': merged_2019_2020_same_ans_ds,
                }
    return datasets


def get_datasets_stats_df(datasets):
    stats_df = pd.DataFrame()
    for k in datasets:
        train_len = len(datasets[k]['train'])
        valid_len = len(datasets[k]['valid'])
        test_len = len(datasets[k]['test'])
        all_len = train_len + valid_len + test_len
        k_series = pd.Series({'Train': train_len, 'Valid': valid_len, 'Test': test_len, 'Total': all_len}, name=k)
        stats_df = stats_df.append(k_series)
    datasets_stats_df = stats_df.astype(int)[['Train', 'Valid', 'Test', 'Total']]
    return datasets_stats_df


def get_merged_19_20_data(all_df_19, df_train_train_20, df_train_val_20, df_valid_20):
    merged_19_20_train = pd.concat([all_df_19, df_train_train_20])
    merged_19_20_valid = df_train_val_20
    merged_19_20_test = df_valid_20
    merged_19_20_train['dataset_type'] = 'train'
    merged_19_20_valid['dataset_type'] = 'valid'
    merged_19_20_test['dataset_type'] = 'test'
    merged_19_20_all = pd.concat([merged_19_20_train, merged_19_20_valid, merged_19_20_test], axis=0, sort=False)
    merged_2019_2020_ds = {'all': merged_19_20_all, 'test': merged_19_20_test, 'train': merged_19_20_train,
                         'valid': merged_19_20_valid}
    return merged_2019_2020_ds

def get_merged_19_20_data_same_ans(all_df_19, df_train_train_20, df_train_val_20, df_valid_20, all_df_20):
    all_2020_answers = set(all_df_20['answer'].values)
    all_df_19_with_same_answers = all_df_19[all_df_19['answer'].isin(all_2020_answers)]
    merged_19_20_train = pd.concat([all_df_19_with_same_answers, df_train_train_20])
    merged_19_20_valid = df_train_val_20
    merged_19_20_test = df_valid_20
    merged_19_20_train['dataset_type'] = 'train'
    merged_19_20_valid['dataset_type'] = 'valid'
    merged_19_20_test['dataset_type'] = 'test'
    merged_19_20_all = pd.concat([merged_19_20_train, merged_19_20_valid, merged_19_20_test], axis=0, sort=False)
    merged_2019_2020_ds = {'all': merged_19_20_all, 'test': merged_19_20_test, 'train': merged_19_20_train,
                         'valid': merged_19_20_valid}
    return merged_2019_2020_ds


def get_trainable_2020_data(df_train_train_20, df_train_val_20, df_valid_20):
    trainable_20_train = df_train_train_20
    trainable_20_valid = df_train_val_20
    trainable_20_test = df_valid_20
    trainable_20_train['dataset_type'] = 'train'
    trainable_20_valid['dataset_type'] = 'valid'
    trainable_20_test['dataset_type'] = 'test'
    trainable_20_all = pd.concat([trainable_20_train, trainable_20_valid, trainable_20_test], axis=0, sort=False)
    trainable_2020_ds = {'all': trainable_20_all, 'test': trainable_20_test, 'train': trainable_20_train, 'valid': trainable_20_valid}
    return trainable_2020_ds


def get_2019_data():
    df_train = read_qa_pairs(dataset_type='train', images=False)
    df_valid = read_qa_pairs(dataset_type='valid', images=False)
    df_test = read_qa_pairs(dataset_type='test', images=False)
    df_valid['dataset_type'] = 'valid'
    all_df = pd.concat([df_train, df_valid, df_test], axis=0, sort=False)
    df_train = df_train[df_train['category'] == 'abnormality']
    df_valid = df_valid[df_valid['category'] == 'abnormality']
    df_test = df_test[df_test['category'] == 'abnormality']
    all_df = all_df[all_df['category'] == 'abnormality']
    return all_df, df_test, df_train, df_valid

def get_2020_data():
    df_train = read_qa_pairs_2020(dataset_type='train')
    df_valid = read_qa_pairs_2020(dataset_type='valid')
    df_test = read_qa_pairs_2020(dataset_type='test')
    all_df = pd.concat([df_train, df_valid, df_test], axis=0, sort=False)
    return all_df, df_test, df_train, df_valid


def produce_answers_distribution_plots(all_df, dataset_name):
    fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(15, 15))

    stats_df = pd.DataFrame()

    cat = 'abnormality'
    df_cat = all_df[all_df['category'] == cat]
    df_cat_train = df_cat[df_cat['dataset_type'] == 'train']['answer'].value_counts()
    df_cat_valid = df_cat[df_cat['dataset_type'] == 'valid']['answer'].value_counts()
    df_cat_test = df_cat[df_cat['dataset_type'] == 'test']['answer'].value_counts()

    train_answers = set(df_cat_train.keys())
    valid_answers = set(df_cat_valid.keys())
    test_answers = set(df_cat_test.keys())

    train_valid_test_corr = {
        'Unique train answers': len(train_answers),
        'Unique valid answers': len(valid_answers),
        'Unique test answers': len(test_answers),
        'Train intersection with valid': len(train_answers.intersection(valid_answers)),
        'Train intersection with test': len(train_answers.intersection(test_answers)),
        'Valid intersection with test': len(valid_answers.intersection(test_answers)),
        'In test but not in train': len(test_answers.difference(train_answers)),
        'In test but not in valid': len(test_answers.difference(valid_answers)),
    }
    train_valid_test_corr_series = pd.Series(train_valid_test_corr, name='Answers correlation - train valid test')
    print(train_valid_test_corr_series)
    train_valid_test_corr_series.to_excel(os.path.join(experiment.output_dir, f"Answers correlation - train valid test - {dataset_name}.xlsx"))

    cat_series = pd.Series({'Train': len(df_cat_train), 'Valid': len(df_cat_valid), 'Test': len(df_cat_test)}, name=cat)
    df_cat_train.plot.bar(ax=axes[0])
    df_cat_valid.plot.bar(ax=axes[1])
    if len(df_cat_test) > 0:
        df_cat_test.plot.bar(ax=axes[2])

    fig.suptitle(f"{cat} Answers distriubtion", fontsize=16)

    row_titles = ['Train', 'Valid', 'Test']
    for ax, row in zip(axes, row_titles):
        ax.set_ylabel(row, rotation=0, size='large', labelpad=40)

    plt.tight_layout()
    plt.savefig(experiment.log_dir + os.sep + f"{cat} answers distribution - {dataset_name}" + '.png')

    stats_df = stats_df.append(cat_series)

    stats_df = stats_df[['Train', 'Valid', 'Test']]
    stats_df = stats_df.astype(int)
    print("Answer distribution")
    print(stats_df)
    print()
    stats_df.to_excel(os.path.join(experiment.output_dir, f"Answer distribution - {dataset_name}.xlsx"))




def get_line_parts(line, test=False):
    if test:
        qa_id, category, question, answer = line.split("|")
        return {"qa_id": qa_id, "category": category, "question": question, "answer": answer}
    else:
        qa_id, question, answer = line.split("|")
        return {"qa_id": qa_id, "question": question, "answer": answer}

def get_line_parts_2020(line, test=False):
    if test:
        qa_id, question= line.split("|")
        return {"qa_id": qa_id, "question": question}
    else:
        qa_id, question, answer = line.split("|")
        return {"qa_id": qa_id, "question": question, "answer": answer}


def read_qa_pairs(dataset_type, images=False):
    dir_p = dataset_type_to_dir_map[dataset_type]
    categories_names_map = {'abnormality': 'C4_Abnormality'}

    if dataset_type != 'test':
        df = pd.DataFrame()
        dir_p += os.sep + 'QAPairsByCategory'
        for cat_key, cat_value, in categories_names_map.items():
            dataset_type_to_read = deepcopy(dataset_type)
            if dataset_type == 'valid':
                dataset_type_to_read = 'val'
            # dataset_type_to_read = dataset_type_to_read.capitalize()
            cat_p = dir_p + os.sep + cat_value + "_" + dataset_type_to_read + ".txt"
            with open(cat_p, encoding='utf-8') as f:
                lines = [x.rstrip('\n') for x in f.readlines()]
            lines = [get_line_parts(line) for line in lines]
            cat_df = pd.DataFrame(lines)
            cat_df['category'] = cat_key
            df = pd.concat([df, cat_df])
            # all_categories_lines[cat] = lines
    else:
        dir_p += os.sep + 'VQAMed2019_Test_Questions_w_Ref_Answers.txt'
        with open(dir_p) as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts(line, test=True) for line in lines]
        abnormality_lines = [line for line in lines if line['category'] == 'abnormality']
        df = pd.DataFrame(abnormality_lines)

    # print(f"\n{dataset_type.capitalize()} dataframe ({len(df)} items)")
    df['dataset_type'] = dataset_type.lower()
    df['year'] = 2019
    return df

def read_qa_pairs_2020(dataset_type):
    dir_p = dataset_type_to_dir_map_2020[dataset_type]

    if dataset_type != 'test':
        df = pd.DataFrame()
        dataset_type_to_read = deepcopy(dataset_type)
        if dataset_type == 'valid':
            dataset_type_to_read = 'val'
        dataset_type_to_read = dataset_type_to_read.capitalize()
        cat_p = os.path.join(dir_p, f"VQAnswering_2020_{dataset_type_to_read}_QA_pairs.txt")
        with open(cat_p, encoding='utf-8') as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts_2020(line) for line in lines]
        cat_df = pd.DataFrame(lines)
        df = pd.concat([df, cat_df])
        # all_categories_lines[cat] = lines
    else:
        cat_p = os.path.join(dir_p, "Task1-2020-VQAnswering-Test-Questions.txt")
        with open(cat_p) as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts_2020(line, test=True) for line in lines]
        df = pd.DataFrame(lines)

    # print(f"\n{dataset_type.capitalize()} dataframe ({len(df)} items)")
    df['dataset_type'] = dataset_type.lower()
    df['category'] = 'abnormality'
    df['year'] = 2020
    return df

if __name__ == '__main__':
    experiment = Experiment(name="Abnormality Data Exploration", export_output=True)
    main()
    experiment.end()
