import pandas as pd
from keras.callbacks import EarlyStopping
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
import numpy as np

from constants import dataset_type_to_dir_map as dataset_type_to_dir_map_2019
from src.Experiment import Experiment
from src.abnormality_category_2020_data.data_exploration_abnormality import get_datasets, dataset_type_to_dir_map_2020, \
    all_images_2020_path, all_images_2019_path
from src.model_experiments.models_constructors.multimodal_models_constructors import *
from src.utils import prepare_labels_to_fit, get_y_pred_and_fixed_y_test, \
    get_metrics_for_pred_and_truth, export_results, get_X_images, plot_history, load_image

DEBUG = False
EPOCHS = 30 if not DEBUG else 3
TOKENIZER_NUM_WORDS = 100
EMBEDDING_DIM = 25

print(f'DEBUG: {DEBUG}, EPOCHS: {EPOCHS}')

os.environ["CUDA_VISIBLE_DEVICES"] = "5"

dataset_name = 'merged_2019_2020_same_ans' # 2019, trainable_2020, merged_2019_2020, merged_2019_2020_same_ans

experiment = Experiment(name=f"Abnormality Multi Modal Model - {dataset_name}", export_output=True, deep_learning=True)

print(f"dataset_name: {dataset_name}")


def main():

    datasets = get_datasets()
    dataset = datasets[dataset_name]
    df_all, df_train, df_valid, df_test = dataset['all'], dataset['train'], dataset['valid'], dataset['test']

    models_constructors_funcs = [prepare_cheap_multi_modal_model, prepare_cheap_multi_modal_model_multiply,
                                 prepare_cheap_multi_modal_model_multiply_batch_norm_after_mult,
                                 prepare_generic_keras_model, prepare_easy_vqa_model, prepare_basic_multimodal_model,
                                 prepare_advanced_resnet_multimodal_model]

    # models_constructors_funcs = [prepare_cheap_multi_modal_model]

    scores_dict = {}

    for model_constructor in models_constructors_funcs:
        model_doc = model_constructor.__doc__
        experiment.runtime_logger.log_task_start(model_doc)
        model_scores_df = get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test)
        print(model_doc)
        print(model_scores_df)
        scores_dict[model_doc] = model_scores_df
        experiment.runtime_logger.log_task_end(model_doc)

    export_results(experiment.output_dir, scores_dict)

    print("Done")

def get_image(qa_id, year, dataset_type):
    pic_path = qa_id + '.jpg'
    if year == 2019:
        dir_p = all_images_2019_path
    else:
        dir_p = all_images_2020_path
    try:
        image_p = os.path.join(dir_p, pic_path)
        image = load_image(image_p)
    except Exception as ex:
        print("CAUGHT EX")
    return image

def get_X_images(df):
    X = df.apply(lambda x: get_image(x['qa_id'], x['year'], x['dataset_type']), axis=1)
    X = np.array(list(X.values))
    return X


def get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test):
    cat = 'abnormality'

    df_train_cat, df_valid_cat, df_test_cat, X_train, X_valid, X_test, \
    y_train_orig, y_valid_orig, y_test_orig, vocab_size, embedding_dim, maxlen = get_train_valid_test_data(cat, df_test, df_train, df_valid)
    y_train_dummies, y_valid_dummies = prepare_labels_to_fit(y_train_orig, y_valid_orig)
    cat_scores_series, y_pred, y_test_fixed = evaluate_multimodal_model(model_constructor, X_train, X_valid, X_test, cat, df_train_cat,
                                                  y_test_orig, y_train_dummies, y_valid_dummies, vocab_size,
                                                  embedding_dim, maxlen)

    return cat_scores_series



def evaluate_multimodal_model(model_constructor, X_train, X_valid, X_test, cat, df_train_cat, y_test_orig,
                              y_train_dummies, y_valid_dummies, vocab_size, embedding_dim, maxlen):
    image_input_shape = X_train['images'][0].shape
    text_input_shape = X_train['questions'][0].shape
    num_outputs = len(set(y_train_dummies.columns))
    model = model_constructor(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)
    history = model.fit([X_train['questions'], X_train['images']], y_train_dummies, epochs=EPOCHS, batch_size=32,
                        verbose=False,
                        validation_data=([X_valid['questions'], X_valid['images']], y_valid_dummies),
                        callbacks=[EarlyStopping(monitor='val_loss', mode='min')])
    # history = model.fit([X_train['questions'], X_train['images']], y_train_dummies, epochs=EPOCHS, batch_size=32,
    #                     verbose=False,
    #                     validation_data=([X_valid['questions'], X_valid['images']], y_valid_dummies))
    plot_history(experiment.dl_plots, history, model_constructor.__doc__ + " " + cat)
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test([X_test['questions'], X_test['images']], model, y_test_orig, y_train_dummies)
    acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
    ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
    return ser, y_pred, y_test_fixed


def get_train_valid_test_data(cat, df_test, df_train, df_valid):

    if DEBUG:
        df_train = df_train.head(20)
        df_valid = df_valid.head(20)
        df_test = df_test.head(20)

    X_train_images = get_X_images(df_train)
    X_valid_images = get_X_images(df_valid)
    X_test_images = get_X_images(df_test)

    X_train_questions_orig, X_valid_questions_orig, X_test_questions_orig = df_train['question'].values, \
                                                                            df_valid['question'].values, \
                                                                            df_test['question'].values
    maxlen = get_maximum_question_length(cat, df_train, df_test)
    tokenizer = Tokenizer(num_words=TOKENIZER_NUM_WORDS)
    tokenizer.fit_on_texts(X_train_questions_orig)
    X_train_questions = tokenizer.texts_to_sequences(X_train_questions_orig)
    X_valid_questions = tokenizer.texts_to_sequences(X_valid_questions_orig)
    X_test_questions = tokenizer.texts_to_sequences(X_test_questions_orig)
    vocab_size = len(tokenizer.word_index) + 1  # Adding 1 because of reserved 0 index
    X_train_questions = pad_sequences(X_train_questions, padding='post', maxlen=maxlen)
    X_valid_questions = pad_sequences(X_valid_questions, padding='post', maxlen=maxlen)
    X_test_questions = pad_sequences(X_test_questions, padding='post', maxlen=maxlen)
    embedding_dim = EMBEDDING_DIM

    y_train_orig, y_valid_orig, y_test_orig = df_train['answer'].values, df_valid['answer'].values, df_test[
        'answer'].values

    X_train = {'questions': X_train_questions, 'images': X_train_images}
    X_valid = {'questions': X_valid_questions, 'images': X_valid_images}
    X_test = {'questions': X_test_questions, 'images': X_test_images}

    return df_train, df_valid, df_test, X_train, X_valid, X_test, y_train_orig, y_valid_orig, y_test_orig, vocab_size, embedding_dim, maxlen

def get_maximum_question_length(cat, df_train_cat, df_valid_cat):
    all_questions = list(df_train_cat['question'].values) + list(df_valid_cat['question'])
    all_questions_len = [len(w) for w in all_questions]
    maxlen = max(all_questions_len)
    return maxlen



if __name__ == '__main__':
    main()
    experiment.end()
