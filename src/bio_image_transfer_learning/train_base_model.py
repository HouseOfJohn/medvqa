import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras import layers, Model
from keras.applications import ResNet50
from keras.callbacks import EarlyStopping

from constants import data_dir

medpix_crawl_dir = data_dir + os.sep + "medpix" + os.sep + "data_crawl"
images_dir = data_dir + os.sep + "medpix" + os.sep + "images"

DEBUG = False

def main():
    df = pd.read_excel(medpix_crawl_dir + os.sep + "search_df.xlsx")

    if DEBUG:
        df = df.head(100)

    image_path_col = 'cached_images_path'
    interesting_columns = ['title', 'diagnosis', 'imaging_modality_from_text', 'image_plane']

    title_num_outpus, diagnosis_num_outpus = explore_cols(df, interesting_columns)
    df['title'].fillna('nan', inplace=True)
    df['diagnosis'].fillna('nan', inplace=True)

    y_title = pd.get_dummies(df['title'].values)
    y_diagnosis = pd.get_dummies(df['diagnosis'].values)

    X = get_X(df, image_path_col)

    model = build_double_loss_model(title_num_outpus, diagnosis_num_outpus)

    epochs = 20 if not DEBUG else 1

    history = model.fit(X,
                  {"title_output": y_title, "diagnosis_output": y_diagnosis},
                  validation_split=0.2,
                  epochs=epochs,
                  verbose=1)

    model_name = 'base_resnet_double_loss.h5' if not DEBUG else 'base_resnet_double_loss_DEBUG.h5'
    model.save(data_dir + os.sep + 'medpix' + os.sep + 'trained_models' + os.sep + model_name)
    print(history)
    plot_history_two_losses(history, acc_loss_name='title_output')
    plot_history_two_losses(history, acc_loss_name='diagnosis_output')

    print("Done")

def plot_history_two_losses(history, acc_loss_name):

    loss_name = acc_loss_name + "_loss"
    loss_val_name = 'val_' + acc_loss_name + "_loss"

    acc_name = acc_loss_name + "_acc"
    acc_val_name = 'val_' + acc_loss_name + "_acc"

    acc = history.history[acc_name]
    val_acc = history.history[acc_val_name]
    loss = history.history[loss_name]
    val_loss = history.history[loss_val_name]
    x = range(1, len(acc) + 1)

    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    plt.plot(x, acc, 'b', label='Training acc')
    plt.plot(x, val_acc, 'r', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Training loss')
    plt.plot(x, val_loss, 'r', label='Validation loss')
    plt.title(f'{acc_loss_name} - Training and validation loss')
    plt.legend()
    plt.show()

def get_X(df, image_path_col):
    df['img'] = df[image_path_col].apply(lambda p: load_image(p))
    all_df_imgs = []
    for im in df['img'].values:
        all_df_imgs.append(list(im))
    X = np.array(all_df_imgs)
    return X


def load_image(infilename) :
    img_p = images_dir + os.sep + infilename.split("/")[-1]
    image = cv2.imread(img_p)
    resized = cv2.resize(image, (224, 224), interpolation=cv2.INTER_AREA)
    return resized


def build_double_loss_model(title_num_outpus, diagnosis_num_outpus):
    input_shape = (224, 224, 3)
    img_input = layers.Input(shape=input_shape)

    conv_base_no_top = ResNet50(include_top=False)
    conv_base_no_top.trainable = True

    average_pool_layer = layers.GlobalAveragePooling2D(name='avg_pool')
    resnet_output = average_pool_layer(conv_base_no_top(img_input))

    title_output = layers.Dense(title_num_outpus, activation='softmax', name='title_output')(resnet_output)
    diagnosis_output = layers.Dense(diagnosis_num_outpus, activation='softmax', name='diagnosis_output')(resnet_output)
    model = Model(
        inputs=img_input,
        outputs=[title_output, diagnosis_output],
        name="resnet_double_model")
    losses = {
        "title_output": "categorical_crossentropy",
        "diagnosis_output": "categorical_crossentropy",
    }
    loss_weights = {"title_output": 1.0, "diagnosis_output": 1.0}
    model.compile(optimizer='rmsprop', loss=losses, loss_weights=loss_weights,
                  metrics=["accuracy"])
    return model


def explore_cols(df, interesting_columns):
    print(f"df len: {len(df)}")
    for c in interesting_columns:
        number_of_missing_elements_in_col = len(df[df[c].isna()])
        number_of_unique_items = len(set(df[c].values))
        print(
            f"col: {c}, number_of_missing_elements_in_col: {number_of_missing_elements_in_col}, number_of_unique_items: {number_of_unique_items}")
    title_num_outpus = len(set(df['title'].values))
    diagnosis_num_outpus = len(set(df['diagnosis'].values))
    return title_num_outpus, diagnosis_num_outpus


def merge_files():
    all_df = pd.DataFrame()
    for fname in os.listdir(medpix_crawl_dir):
        df = pd.read_excel(medpix_crawl_dir + os.sep + fname)
        all_df = pd.concat([all_df, df])
        print(f"df : {df.shape}, {all_df.shape}")
    all_df.to_excel(medpix_crawl_dir + os.sep + 'search_df.xlsx', index=False)
    return all_df



if __name__ == '__main__':
    # merge_files()
    main()

