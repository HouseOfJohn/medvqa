import os

import cv2
import numpy as np
import pandas as pd
from keras import layers, Model
from keras.applications import ResNet50

from constants import data_dir
from src.models_utils import plot_history

medpix_crawl_dir = data_dir + os.sep + "medpix" + os.sep + "data_crawl"
images_dir = data_dir + os.sep + "medpix" + os.sep + "images"

DEBUG = False

def main():
    df = pd.read_excel(medpix_crawl_dir + os.sep + "search_df.xlsx")

    if DEBUG:
        df = df.head(100)

    image_path_col = 'cached_images_path'
    interesting_columns = ['title', 'diagnosis', 'imaging_modality_from_text', 'image_plane']

    modality_num_outputs = explore_cols(df, interesting_columns)
    df['imaging_modality_from_text'].fillna('nan', inplace=True)

    y = pd.get_dummies(df['imaging_modality_from_text'].values)

    X = get_X(df, image_path_col)

    model = build_single_loss_model(modality_num_outputs)

    epochs = 20 if not DEBUG else 1

    history = model.fit(X, y, validation_split=0.2, epochs=epochs, verbose=1)

    model_name = 'base_resnet_single_modality_loss.h5' if not DEBUG else 'base_resnet_single_modality_loss_DEBUG.h5'
    model.save(data_dir + os.sep + 'medpix' + os.sep + 'trained_models' + os.sep + model_name)
    print(history)
    plot_history(history)

    print("Done")


def get_X(df, image_path_col):
    df['img'] = df[image_path_col].apply(lambda p: load_image(p))
    all_df_imgs = []
    for im in df['img'].values:
        all_df_imgs.append(list(im))
    X = np.array(all_df_imgs)
    return X


def load_image(infilename) :
    img_p = images_dir + os.sep + infilename.split("/")[-1]
    image = cv2.imread(img_p)
    resized = cv2.resize(image, (224, 224), interpolation=cv2.INTER_AREA)
    return resized


def build_single_loss_model(modality_num_outputs):
    input_shape = (224, 224, 3)
    img_input = layers.Input(shape=input_shape)

    conv_base_no_top = ResNet50(include_top=False)
    average_pool_layer = layers.GlobalAveragePooling2D(name='avg_pool')
    resnet_output = average_pool_layer(conv_base_no_top(img_input))

    modality_output = layers.Dense(modality_num_outputs, activation='softmax', name='modality_output')(resnet_output)

    model = Model(
        inputs=img_input,
        outputs=modality_output,
        name="modality_output")

    model.compile(optimizer='rmsprop', loss='categorical_crossentropy',
                  metrics=["accuracy"])
    return model


def explore_cols(df, interesting_columns):
    print(f"df len: {len(df)}")
    for c in interesting_columns:
        number_of_missing_elements_in_col = len(df[df[c].isna()])
        number_of_unique_items = len(set(df[c].values))
        print(
            f"col: {c}, number_of_missing_elements_in_col: {number_of_missing_elements_in_col}, number_of_unique_items: {number_of_unique_items}")
    # title_num_outpus = len(set(df['title'].values))
    modality_num_outputs = len(set(df['imaging_modality_from_text'].values))
    return modality_num_outputs


def merge_files():
    all_df = pd.DataFrame()
    for fname in os.listdir(medpix_crawl_dir):
        df = pd.read_excel(medpix_crawl_dir + os.sep + fname)
        all_df = pd.concat([all_df, df])
        print(f"df : {df.shape}, {all_df.shape}")
    all_df.to_excel(medpix_crawl_dir + os.sep + 'search_df.xlsx', index=False)
    return all_df



if __name__ == '__main__':
    # merge_files()
    main()

