import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from nltk.translate.bleu_score import corpus_bleu
from sklearn.metrics import accuracy_score, f1_score

from constants import data_dir, dataset_type_to_dir_map, categories_names_map


def plot_history(plots_dir, history, doc=None):
    acc_name = 'accuracy' if 'accuracy' in history.history else 'acc'
    acc = history.history[acc_name]
    val_acc = history.history['val_' + acc_name]
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    x = range(1, len(acc) + 1)

    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    plt.plot(x, acc, 'b', label='Training acc')
    plt.plot(x, val_acc, 'r', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Training loss')
    plt.plot(x, val_loss, 'r', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    if doc:
        plot_p = os.path.join(plots_dir, doc + ' plot.png')
        plt.savefig(plot_p)
        # print(f"saved plot {plot_p}")
    else:
        plt.savefig('val_and_acc_plot.png')
        plt.show()
    # plt.show()


def fix_truth_by_brackets(pred, truth):
    if "(" not in truth:
        return truth
    else:
        if pred not in truth or '(' in pred:
            return truth
        else:
            text_in_brackets = truth[truth.find("(") + 1:truth.find(")")]
            fixed_truth = truth.replace(f"({text_in_brackets})", '').strip()
            return fixed_truth


def fix_y_test(y_test_orig, y_pred):
    fixed_y_test = []

    for pred, truth in zip(y_pred, y_test_orig):
        fixed_truth = fix_truth_by_brackets(pred, truth)
        fixed_truth = fix_truth_by_hashtag(pred, fixed_truth)
        # if truth != fixed_truth:
            # print(f"truth: {truth} changed to -> {fixed_truth}, pred: {pred}")
            # print()
        fixed_y_test.append(fixed_truth)
    return fixed_y_test


def fix_truth_by_hashtag(pred, truth):
    if '#' not in truth:
        fixed_truth = truth
    else:
        fixed_truth = get_fixed_truth_for_hashtag(pred, truth)
    return fixed_truth


def get_fixed_truth_for_hashtag(pred, truth):
    fixed_truth = None
    truth_parts = truth.split("#")
    found_t = False
    for t in truth_parts:
        if t == pred:
            fixed_truth = t
            found_t = True
            break
    if not found_t:
        fixed_truth = truth_parts[0]
    return fixed_truth


def prepare_labels_to_fit(y_train_orig, y_valid_orig):
    y_train_dummies = pd.get_dummies(y_train_orig)
    y_valid_dummies = pd.DataFrame(pd.get_dummies(y_valid_orig), columns=y_train_dummies.columns)
    y_valid_dummies.fillna(0, inplace=True)
    return y_train_dummies, y_valid_dummies

def prepare_labels_to_fit_with_test(y_train_orig, y_valid_orig, y_test_orig):
    y_train_dummies = pd.get_dummies(y_train_orig)

    y_valid_dummies = pd.DataFrame(pd.get_dummies(y_valid_orig), columns=y_train_dummies.columns)
    y_valid_dummies.fillna(0, inplace=True)

    y_test_dummies = pd.DataFrame(pd.get_dummies(y_test_orig), columns=y_train_dummies.columns)
    y_test_dummies.fillna(0, inplace=True)
    return y_train_dummies, y_valid_dummies, y_test_dummies

def get_data_dataframes(to_csv=False):
    df_train = read_qa_pairs(dataset_type='train')
    df_valid = read_qa_pairs(dataset_type='valid')
    df_test = read_qa_pairs(dataset_type='test')
    all_df = pd.concat([df_train, df_valid, df_test], axis=0, sort=False)
    print(f"merged data at len: {len(all_df)}")

    if to_csv:
        df_train.to_csv(data_dir + os.sep + "train.csv", index=False)
        df_valid.to_csv(data_dir + os.sep + "valid.csv", index=False)
        df_test.to_csv(data_dir + os.sep + "test.csv", index=False)
        print(f"Wrote to_csv {data_dir}")

    return all_df, df_train, df_valid, df_test

def read_qa_pairs(dataset_type):
    dir_p = dataset_type_to_dir_map[dataset_type]

    if dataset_type != 'test':
        if dataset_type == 'valid':
            dataset_type = 'val'
        df = pd.DataFrame()
        dir_qa_pic = dir_p + os.sep + 'QAPairsByCategory'
        for cat_key, cat_value, in categories_names_map.items():
            cat_p = dir_qa_pic + os.sep + cat_value + "_" + dataset_type + ".txt"
            with open(cat_p, encoding='utf-8') as f:
                lines = [x.rstrip('\n') for x in f.readlines()]
            lines = [get_line_parts(line) for line in lines]
            cat_df = pd.DataFrame(lines)
            cat_df['category'] = cat_key
            df = pd.concat([df, cat_df])
            # all_categories_lines[cat] = lines
    else:
        dir_p += os.sep + 'VQAMed2019_Test_Questions_w_Ref_Answers.txt'
        with open(dir_p) as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts(line, test=True) for line in lines]
        df = pd.DataFrame(lines)

    # print(f"\nGot {dataset_type} dataframe at len: {len(df)}")
    # print(df['category'].value_counts())

    df['dataset_type'] = dataset_type
    return df

def get_line_parts(line, test=False):
    if test:
        qa_id, category, question, answer = line.split("|")
        pic_p = qa_id + ".jpg"
        return {"qa_id": qa_id, "pic_path": pic_p, "category": category, "question": question, "answer": answer}
    else:
        qa_id, question, answer = line.split("|")
        pic_p = qa_id + ".jpg"
        return {"qa_id": qa_id, "pic_path": pic_p, "question": question, "answer": answer}

def get_y_pred_and_fixed_y_test(X_test, model, y_test_orig, y_train_dummies):
    # y_pred_classes = model.predict_classes(X_test, verbose=False)
    y_pred_probas = model.predict(X_test, verbose=False)
    y_pred_classes = np.argmax(y_pred_probas, axis=1)
    y_pred = [y_train_dummies.columns[pred_idx] for pred_idx in y_pred_classes]
    y_test_fixed = fix_y_test(y_test_orig, y_pred)
    return y_pred, y_test_fixed

def get_metrics_for_pred_and_truth(y_pred, y_test_fixed):
    acc = round(accuracy_score(y_test_fixed, y_pred), 2)
    f1_score_val = round(f1_score(y_test_fixed, y_pred, average='weighted'), 2)
    bleu_score = round(corpus_bleu([[x] for x in y_test_fixed], y_pred), 2)
    return acc, bleu_score, f1_score_val

def export_results(output_dir, scores_dict):
    for k, v in scores_dict.items():
        v.to_excel(os.path.join(output_dir, k + ".xlsx"))
        print(k)
        print(v)
        print()

def add_averages_to_scores_df(model_scores_df):
    mean = model_scores_df.mean().apply(lambda x: round(x, 2))
    mean_without_abnormality = model_scores_df.drop(['Abnormality']).mean().apply(lambda x: round(x, 2))
    model_scores_df = model_scores_df.append(pd.Series(mean, name='Average'))
    model_scores_df = model_scores_df.append(pd.Series(mean_without_abnormality, name='Average without abnormality'))
    return model_scores_df


def get_X_images(df, dataset_type):
    dir_p = dataset_type_to_dir_map[dataset_type]
    dir_pics_p = dir_p + os.sep + 'images'
    X = df['pic_path'].apply(lambda p: load_image(dir_pics_p + os.sep + p))
    X = np.array(list(X.values))
    return X

def load_image(infilename) :
    image = cv2.imread(infilename)
    resized = cv2.resize(image, (224, 224), interpolation=cv2.INTER_AREA)
    return resized

if __name__ == '__main__':
    y_test_orig = [['noncontrast (mri)']]
    y_pred = [['noncontrast']]
    ans = fix_y_test(y_test_orig, y_pred)
    print(ans)