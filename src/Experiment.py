import json
import os
import sys
import time
from datetime import datetime
from distutils.dir_util import copy_tree

from constants import data_dir


class Experiment:
    def __init__(self, name="", export_output=False, deep_learning=False):
        self.name = name
        self.export_output = export_output
        if self.export_output:
            self.log_dir, self.output_dir = self.create_output_dirs()
            sys.stdout = Logger(self.log_dir)
            self.runtime_logger = RuntimeLogger(log_dir=self.log_dir, export_output=True)

            if deep_learning:
                self.create_deep_learning_dirs()
        else:
            self.runtime_logger = RuntimeLogger(export_output=False)
        self.start()
        print(f"Running experiment: {name}")

    def create_deep_learning_dirs(self):
        self.dl_dir = os.path.join(self.log_dir, 'deep_learning')
        self.dl_params_dir = os.path.join(self.dl_dir, 'params')
        self.dl_tensorboard = os.path.join(self.dl_dir, 'tensorboard')
        self.dl_plots = os.path.join(self.dl_dir, 'plots')
        for dir_p in [self.dl_dir, self.dl_params_dir, self.dl_tensorboard, self.dl_plots]:
            os.mkdir(dir_p)

    def create_output_dirs(self):
        start_time_str = datetime.today().strftime('%d_%m_%Y__%H_%M_%S')
        log_dir = os.path.join(data_dir, "experiments_logs", f"log_{start_time_str}_{self.name}")
        os.mkdir(log_dir)
        output_dir = os.path.join(log_dir, 'outputs')
        os.mkdir(output_dir)
        return log_dir, output_dir

    def start(self):
        self.runtime_logger.start()

    def end(self):
        self.runtime_logger.log_experiment_end()
        print(f"Finished experiment {self.name}")

    def export_config(self):
        pass

class Logger(object):
    def __init__(self, output_dir):
        self.terminal = sys.stdout
        self.log = open(os.path.join(output_dir, "logfile.log"), "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass

class RuntimeLogger:
    def __init__(self, log_dir=None, export_output=False):
        self.export_output = export_output
        self.runtime_dict = {}
        self.task_start_dict = {}
        self.log_task_start("global_runtime")
        if self.export_output:
            self.runtime_filepath = os.path.join(log_dir, "runtime.json")

    def start(self):
        self.global_start_time = time.time()

    def log_task_start(self, task_name: str):
        start_task_time = time.time()
        self.task_start_dict[task_name] = start_task_time

    def log_task_end(self, task_name: str):
        task_runtime = round(time.time() - self.task_start_dict[task_name], 3)
        self.runtime_dict.update({task_name: task_runtime})

    def log_experiment_end(self):
        self.log_task_end('global_runtime')
        if self.export_output:
            json.dump(self.runtime_dict,
                      open(self.runtime_filepath, 'w'), indent=4)

if __name__ == '__main__':
    e = Experiment(name="Debug experiment class", export_output=True)
    e.runtime_logger.log_task_start("first_task")
    time.sleep(2.4)
    e.runtime_logger.log_task_end("first_task")
    time.sleep(1.2)

    print("Try")
    e.end()
