from keras.utils import plot_model

from src.model_experiments.models_constructors.multimodal_models_constructors import prepare_cheap_multi_modal_model, \
    prepare_easy_vqa_model, prepare_cheap_multi_modal_model_multiply, prepare_cheap_multi_modal_model_average, \
    prepare_cheap_multi_modal_model_dot, prepare_cheap_multi_modal_model_cdn_1

print("Hello w0rld")

# text_model = prepare_conv1d_model(vocab_size=36, embedding_dim=25, maxlen=55, num_outputs=40)
# print(text_model.summary())
#
# image_model = prepare_cnn_model_exp_hirerchical(input_shape=(224, 224, 3), num_outputs=40)
# print(image_model.summary())

vocab_size = 36
embedding_dim = 25
maxlen = 59
num_outputs = 40
image_input_shape = (224, 224, 3)
text_input_shape = (59, )

# for f in [prepare_cheap_multi_modal_model_multiply]:
#     print(f.__doc__)
#     f(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)
#     print("\n\n")
# joint_model = prepare_cheap_multi_modal_model(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)

# model = prepare_cheap_multi_modal_model_multiply(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)
# plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)

model = prepare_cheap_multi_modal_model_multiply(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)
print(model.summary())

print('done')

