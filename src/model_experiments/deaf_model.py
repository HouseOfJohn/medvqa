import os
from distutils.dir_util import copy_tree

import cv2
import numpy as np
import pandas as pd

from constants import categories_names, data_dir, dataset_type_to_dir_map
from src.Experiment import Experiment
from src.model_experiments.models_constructors.deaf_models_constructors import *
from src.utils import get_data_dataframes, prepare_labels_to_fit, get_y_pred_and_fixed_y_test, \
    get_metrics_for_pred_and_truth, export_results, add_averages_to_scores_df, get_X_images, plot_history

deaf_models_dir = data_dir + os.sep + 'model_experiments' + os.sep + 'deaf_model'
DEBUG = False
EPOCHS = 20 if not DEBUG else 3

print(f'DEBUG: {DEBUG}')
os.environ["CUDA_VISIBLE_DEVICES"] = "6"

experiment = Experiment(name="Deaf Model", export_output=True, deep_learning=True)

def main():
    df_all, df_train, df_valid, df_test = get_data_dataframes()

    # models_constructors_funcs = [prepare_cheap_cnn_model, prepare_cnn_model_exp_hirerchical, prepare_cnn_model_exp_hirerchical_batchnorm, prepare_cnn_model, prepare_cnn_vgg16_model, prepare_cnn_vgg16_model_finetune, resnet18, resnet34, prepare_cnn_resnet_model, prepare_cnn_resnet_model_finetune]
    # models_constructors_funcs = [prepare_cheap_cnn_model, prepare_cnn_model_exp_hirerchical,
    #                              prepare_cnn_model_exp_hirerchical_batchnorm, prepare_cnn_model,
    #                              prepare_cnn_vgg16_model, prepare_cnn_vgg16_model_finetune, resnet18, resnet34]
    models_constructors_funcs = [prepare_cnn_model_exp_hirerchical]

    scores_dict = {}

    for model_constructor in models_constructors_funcs:
        model_doc = model_constructor.__doc__
        experiment.runtime_logger.log_task_start(model_doc)
        model_scores_df = get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test)
        print(model_doc)
        print(model_scores_df)
        scores_dict[model_doc] = model_scores_df
        experiment.runtime_logger.log_task_end(model_doc)

    export_results(experiment.output_dir, scores_dict)

    print("Done")


def get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test):
    model_scores_df = pd.DataFrame()
    for cat in categories_names:
        df_train_cat, df_valid_cat, df_test_cat, X_train_orig, X_valid_orig, X_test_orig, y_train_orig, y_valid_orig, y_test_orig \
            = get_train_valid_test_data(cat, df_test, df_train, df_valid)
        y_train_dummies, y_valid_dummies = prepare_labels_to_fit(y_train_orig, y_valid_orig)
        cat_scores_series = evaluate_vision_model(model_constructor, X_test_orig, X_valid_orig, X_train_orig, cat,
                                                  df_train_cat, y_test_orig, y_train_dummies, y_valid_dummies)
        model_scores_df = model_scores_df.append(cat_scores_series)

    model_scores_df = add_averages_to_scores_df(model_scores_df)

    return model_scores_df



def evaluate_vision_model(model_constructor, X_test_orig, X_valid_orig, X_train_orig, cat, df_train_cat, y_test_orig, y_train_dummies, y_valid_dummies):
    model = model_constructor(input_shape=X_train_orig[0].shape, num_outputs=len(set(df_train_cat['answer'].values)))
    history = model.fit(X_train_orig, y_train_dummies, epochs=EPOCHS, batch_size=32, verbose=False,
                        validation_data=(X_valid_orig, y_valid_dummies),
                        callbacks=[EarlyStopping(monitor='val_loss', mode='min')])
    plot_history(experiment.dl_plots, history, model_constructor.__doc__ + " " + cat)
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test(X_test_orig, model, y_test_orig, y_train_dummies)
    acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
    ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
    return ser


def get_train_valid_test_data(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['category'] == cat]
    df_valid_cat = df_valid[df_valid['category'] == cat]
    df_test_cat = df_test[df_test['category'] == cat]

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    X_train = get_X_images(df_train_cat, dataset_type='train')
    X_valid = get_X_images(df_valid_cat, dataset_type='valid')
    X_test = get_X_images(df_test_cat, dataset_type='test')

    y_train_orig, y_valid_orig, y_test_orig = df_train_cat['answer'].values, df_valid_cat['answer'].values, df_test_cat['answer'].values

    return df_train_cat, df_valid_cat, df_test_cat, X_train, X_valid, X_test, y_train_orig, y_valid_orig, y_test_orig


if __name__ == '__main__':
    main()
    copy_tree(experiment.output_dir, os.path.join(deaf_models_dir, 'outputs'))
    copy_tree(experiment.dl_plots, os.path.join(deaf_models_dir, 'plots'))
    experiment.end()
