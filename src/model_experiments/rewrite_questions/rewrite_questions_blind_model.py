from distutils.dir_util import copy_tree

import cv2
import pandas as pd
from keras.callbacks import EarlyStopping
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer

from constants import categories_names, dataset_type_to_dir_map
from src.Experiment import Experiment
from src.model_experiments.models_constructors.blind_models_constructors import *
from src.parsing.QuestionsParser import QuestionsParser
from src.utils import prepare_labels_to_fit, get_y_pred_and_fixed_y_test, \
    get_metrics_for_pred_and_truth, export_results, fix_y_test, add_averages_to_scores_df, plot_history

blind_models_dir = data_dir + os.sep + 'model_experiments' + os.sep + 'blind_model'
DEBUG = False
EPOCHS = 20 if not DEBUG else 3
EMBEDDING_DIM = 10
SIMPLIFY = True

print(f'DEBUG: {DEBUG}')
os.environ["CUDA_VISIBLE_DEVICES"] = "5"

experiment = Experiment(name=f"Rewrite - Blind Model - Simplify: {SIMPLIFY}, Embedding: {EMBEDDING_DIM}", export_output=True, deep_learning=True)

def main():
    df_train, df_valid, df_test = read_data('train'), read_data('valid'), read_data('test')

    # models_constructors_funcs = [prepare_dense_model, prepare_conv1d_model, prepare_lstm_model, prepare_keras_lstm_attention_model]
    # models_constructors_funcs = [prepare_dense_model]

    scores_dict = {}

    # for model_constructor in models_constructors_funcs:
    #     model_doc = model_constructor.__doc__
    #     experiment.runtime_logger.log_task_start(model_doc)
    #     model_scores_df = get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test)
    #     print(model_doc)
    #     print(model_scores_df)
    #     scores_dict[model_doc] = model_scores_df
    #     experiment.runtime_logger.log_task_end(model_doc)

    run_bow_model(df_test, df_train, df_valid, scores_dict)

    # run_bert_model(df_test, df_train, df_valid, scores_dict)

    export_results(experiment.output_dir, scores_dict)

    print("Done")


def run_bow_model(df_test, df_train, df_valid, scores_dict):
    bow_model_name = "BoW Model"
    experiment.runtime_logger.log_task_start(bow_model_name)
    bow_model_scores_df = get_scores_for_bow_model(df_test, df_train, df_valid)
    scores_dict[bow_model_name] = bow_model_scores_df
    experiment.runtime_logger.log_task_end(bow_model_name)


def run_bert_model(df_test, df_train, df_valid, scores_dict):
    bert_model_name = "BERT Model"
    experiment.runtime_logger.log_task_start(bert_model_name)
    bert_scores_df = get_scores_for_bert(df_test, df_train, df_valid)
    scores_dict[bert_model_name] = bert_scores_df
    experiment.runtime_logger.log_task_end(bert_model_name)


def get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test):
    model_scores_df = pd.DataFrame()
    for cat in ['modality', 'plane', 'organ', 'abnormality']: # ['modality', 'plane', 'organ', 'abnormality']
        X_test, X_train, X_valid, df_train_cat, df_test_cat, maxlen, tokenizer, vocab_size, y_test_orig, y_train_orig, y_valid_orig = tokenize_and_prepare_data(
            cat, df_test, df_train, df_valid)

        print(f"Finished with category: {cat}")

        y_train_dummies, y_valid_dummies = prepare_labels_to_fit(y_train_orig, y_valid_orig)
        cat_scores_series, y_pred = evaluate_text_model(model_constructor, X_test, X_train, X_valid,
                        EMBEDDING_DIM, maxlen, vocab_size,
                        y_test_orig, y_train_orig, y_train_dummies, y_valid_dummies, cat)

        # df_test_cat['y_pred'] = y_pred
        # df_test_misclassification = df_test_cat.query('y_pred != answer')
        # print(f"df_test_misclassification: {len(df_test_misclassification)}")

        model_scores_df = model_scores_df.append(cat_scores_series)

    model_scores_df = add_averages_to_scores_df(model_scores_df)

    return model_scores_df


def evaluate_text_model(model_constructor, X_test, X_train, X_valid,
                        embedding_dim, maxlen, vocab_size,
                        y_test_orig, y_train_orig, y_train_dummies, y_valid_dummies, cat):
    model = model_constructor(vocab_size, embedding_dim, maxlen, num_outputs=len(set(y_train_orig)))
    history = model.fit(X_train, y_train_dummies, epochs=EPOCHS, batch_size=32, verbose=False,
                        validation_data=(X_valid, y_valid_dummies))
    plot_history(experiment.dl_plots, history, model_constructor.__doc__ + " " + cat)
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test(X_test, model, y_test_orig, y_train_dummies)
    acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
    ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
    return ser, y_pred


def get_scores_for_bert(df_test, df_train, df_valid):
    from simpletransformers.classification import ClassificationModel

    bert_scores_df = pd.DataFrame()
    for cat in categories_names:
        df_test_cat, df_train_cat, le, num_labels = prepare_data_for_bert_classification(cat, df_test, df_train,
                                                                                         df_valid)

        model = ClassificationModel('bert', 'bert-base-cased', num_labels=num_labels, use_cuda=False,
                                    args={'reprocess_input_data': True, 'overwrite_output_dir': True})

        model.train_model(df_train_cat)

        predictions, raw_outputs = model.predict(df_test_cat['text'].values)

        y_pred = le.inverse_transform(predictions)
        y_test = df_test_cat['labels'].values

        y_test_fixed = fix_y_test(y_test, y_pred)
        acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
        ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
        bert_scores_df = bert_scores_df.append(ser)

    bert_scores_df = add_averages_to_scores_df(bert_scores_df)
    return bert_scores_df


def prepare_data_for_bert_classification(cat, df_test, df_train, df_valid):
    from sklearn.preprocessing import LabelEncoder

    df_train_cat = df_train[df_train['question_type'] == cat]
    df_train_cat = df_train_cat[['question', 'answer']]
    df_train_cat.rename(columns={"question": "text", "answer": "labels"}, inplace=True)
    df_valid_cat = df_valid[df_valid['question_type'] == cat]
    df_valid_cat = df_valid_cat[['question', 'answer']]
    df_valid_cat.rename(columns={"question": "text", "answer": "labels"}, inplace=True)
    df_test_cat = df_test[df_test['question_type'] == cat]
    df_test_cat = df_test_cat[['question', 'answer']]
    df_test_cat.rename(columns={"question": "text", "answer": "labels"}, inplace=True)

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    le = LabelEncoder()
    all_answers = list(set(list(df_train_cat['labels'].values) + list(df_valid_cat['labels'].values)))
    num_labels = len(all_answers)
    le.fit(all_answers)
    df_train_cat['labels'] = list(le.transform(df_train_cat['labels'].values))
    df_valid_cat['labels'] = list(le.transform(df_valid_cat['labels'].values))
    return df_test_cat, df_train_cat, le, num_labels


def get_scores_for_bow_model(df_test, df_train, df_valid):
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.linear_model import LogisticRegression

    bow_model_scores_df = pd.DataFrame()
    for cat in categories_names:
        df_train_cat, df_test_cat, X_train_orig, X_test_orig, y_train_orig, y_test_orig \
            = get_train_test_data_orig_text(cat, df_test, df_train, df_valid)
        vectorizer = CountVectorizer()
        vectorizer.fit(X_train_orig)
        X_train = vectorizer.transform(X_train_orig)
        X_test = vectorizer.transform(X_test_orig)
        classifier = LogisticRegression()
        classifier.fit(X_train, y_train_orig)
        y_pred = classifier.predict(X_test)
        y_test_fixed = fix_y_test(y_test_orig, y_pred)
        acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
        ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
        bow_model_scores_df = bow_model_scores_df.append(ser)

    bow_model_scores_df = add_averages_to_scores_df(bow_model_scores_df)

    return bow_model_scores_df


def get_train_valid_test_data(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['question_type'] == cat]
    df_valid_cat = df_valid[df_valid['question_type'] == cat]
    df_test_cat = df_test[df_test['question_type'] == cat]

    all_df = pd.concat([df_train_cat, df_valid_cat, df_test_cat])
    question_parser = QuestionsParser.by_name(f"{cat}_parser")()
    question_parser.create_questions_groups(all_df)

    df_train_cat = question_parser.rewrite_questions(df_train_cat, simplify_question=SIMPLIFY)
    df_valid_cat = question_parser.rewrite_questions(df_valid_cat, simplify_question=SIMPLIFY)
    df_test_cat = question_parser.rewrite_questions(df_test_cat, simplify_question=SIMPLIFY)

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    X_train_orig, X_valid_orig, X_test_orig = df_train_cat['question'].values, df_valid_cat['question'].values, \
                                              df_test_cat['question'].values
    y_train_orig, y_valid_orig, y_test_orig = df_train_cat['answer'].values, df_valid_cat['answer'].values, \
                                              df_test_cat['answer'].values

    return df_train_cat, df_valid_cat, df_test_cat, X_train_orig, X_valid_orig, X_test_orig, y_train_orig, y_valid_orig, y_test_orig



def read_data(dataset_dtype):
    data_p = data_dir + os.sep + dataset_dtype + ".csv"
    df = pd.read_csv(data_p)
    return df

def tokenize_and_prepare_data(cat, df_test, df_train, df_valid):
    df_train_cat, df_valid_cat, df_test_cat, X_train_orig, X_valid_orig, X_test_orig, y_train_orig, y_valid_orig, y_test_orig = get_train_valid_test_data(
        cat, df_test, df_train, df_valid)

    train_words = set([x.replace("?", "") for x in set(" ".join(X_train_orig).split(" "))])
    valid_words = set([x.replace("?", "") for x in set(" ".join(X_valid_orig).split(" "))])
    test_words = set([x.replace("?", "") for x in set(" ".join(X_test_orig).split(" "))])

    vocab_size = len(train_words.union(valid_words).union(test_words))

    maxlen = get_maximum_question_length(cat, df_train_cat, df_test_cat)
    tokenizer = Tokenizer(num_words=vocab_size) # num of unique words
    tokenizer.fit_on_texts(X_train_orig)
    X_train = tokenizer.texts_to_sequences(X_train_orig)
    X_valid = tokenizer.texts_to_sequences(X_valid_orig)
    X_test = tokenizer.texts_to_sequences(X_test_orig)
    vocab_size = len(tokenizer.word_index) + 1  # Adding 1 because of reserved 0 index
    X_train = pad_sequences(X_train, padding='post', maxlen=maxlen)
    X_valid = pad_sequences(X_valid, padding='post', maxlen=maxlen)
    X_test = pad_sequences(X_test, padding='post', maxlen=maxlen)
    return X_test, X_train, X_valid, df_train_cat, df_test_cat, maxlen, tokenizer, vocab_size, y_test_orig, y_train_orig, y_valid_orig

def get_maximum_question_length(cat, df_train_cat, df_valid_cat):
    all_questions = list(df_train_cat['question'].values) + list(df_valid_cat['question'])
    all_questions_len = [len(w) for w in all_questions]
    maxlen = max(all_questions_len)
    return maxlen

def get_train_test_data_orig_text(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['question_type'] == cat]
    df_valid_cat = df_valid[df_valid['question_type'] == cat]

    df_train_cat = pd.concat([df_train_cat, df_valid_cat])

    df_test_cat = df_test[df_test['question_type'] == cat]
    X_train_orig, X_test_orig = df_train_cat['question'].values, df_test_cat['question'].values
    y_train_orig, y_test_orig = df_train_cat['answer'].values, df_test_cat['answer'].values

    return df_train_cat, df_test_cat, X_train_orig, X_test_orig, y_train_orig, y_test_orig



if __name__ == '__main__':
    main()
    experiment.end()
