import os
from keras import Sequential
from keras import layers, Input, Model
from keras.applications import ResNet50
from keras.optimizers import Adam

from constants import data_dir

optimizer = 'adam'  # adam


def prepare_cheap_multi_modal_model(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim,
                                    maxlen):
    """prepare_cheap_multi_modal_model - concat"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_multiply(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                             embedding_dim, maxlen):
    """Cheap Multi Modal Model - Multiply features"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.multiply([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_concat_same_shape(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                             embedding_dim, maxlen):
    """Cheap Multi Modal Model - Concat same shape features"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model

def prepare_cheap_multi_modal_model_cdn_1(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim,
                                          maxlen):
    """Cheap Multi Modal Model - CDN1"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    # x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    # Current status - x_blind is text tensor, dim of 128, x_deaf is the image tensor, dim of 1200

    x_deaf = LayerNormalization()(x_deaf)
    x_blind = LayerNormalization()(x_blind)

    text_dim = 128
    image_dim = 1200
    text_image_ratio = text_dim / image_dim
    image_text_ratio = image_dim / text_dim

    # s_lang = text_image_ratio
    s_lang = 1
    s_image = image_text_ratio

    x_blind = layers.Lambda(lambda v: v / s_lang)(x_blind)  # Dividing the output of the LN in s_lang
    x_deaf = layers.Lambda(lambda v: v / s_image)(x_deaf)  # Dividing the output of the LN in s_image

    x = layers.concatenate([x_blind, x_deaf])  # concatenating the vectors of 1200 and 128 to a vector of 1328

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(model.summary())
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_cdn_1_multiply(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                                   embedding_dim, maxlen):
    """Cheap Multi Modal Model - CDN1 Multiply"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    from keras_layer_normalization import LayerNormalization

    x_deaf = LayerNormalization()(x_deaf)
    x_blind = LayerNormalization()(x_blind)

    text_dim = 128
    image_dim = 1200
    text_image_ratio = text_dim / image_dim
    image_text_ratio = image_dim / text_dim

    # s_lang = text_image_ratio
    s_lang = 1
    s_image = image_text_ratio


    x_blind = layers.Lambda(lambda x: x / s_lang)(x_blind)
    x_deaf = layers.Lambda(lambda x: x / s_image)(x_deaf)

    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x = layers.multiply([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(model.summary())
    # print(model.summary())
    return model

def prepare_cheap_multi_modal_model_cdn_1_concat_same_shape(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                                   embedding_dim, maxlen):
    """Cheap Multi Modal Model - CDN1 Concat same shape """
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    from keras_layer_normalization import LayerNormalization
    x_deaf = LayerNormalization()(x_deaf)
    x_blind = LayerNormalization()(x_blind)

    text_dim = 128
    image_dim = 1200
    text_image_ratio = text_dim / image_dim
    image_text_ratio = image_dim / text_dim

    # s_lang = text_image_ratio
    s_lang = 1
    s_image = image_text_ratio

    x_blind = layers.Lambda(lambda x: x / s_lang)(x_blind)
    x_deaf = layers.Lambda(lambda x: x / s_image)(x_deaf)

    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x = layers.concatenate([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(model.summary())
    # print(model.summary())
    return model

def prepare_cheap_multi_modal_model_multiply_batch_norm_after_mult(image_input_shape, text_input_shape, num_outputs,
                                                                   vocab_size, embedding_dim, maxlen):
    """Cheap Multi Modal Model - Multiply features - With BatchNorm"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.multiply([x_blind, x_deaf])
    x = layers.BatchNormalization()(x)

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_multiply_extra_dense_after_multiply(image_input_shape, text_input_shape,
                                                                        num_outputs, vocab_size, embedding_dim, maxlen):
    """prepare_cheap_multi_modal_model_multiply_extra_dense_after_multiply"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.multiply([x_blind, x_deaf])
    x = layers.Dense(64, activation='relu')(x)

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_multiply_extra_dense_before_multiply(image_input_shape, text_input_shape,
                                                                         num_outputs, vocab_size, embedding_dim,
                                                                         maxlen):
    """prepare_cheap_multi_modal_model_multiply_extra_dense_before_multiply"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)
    x_deaf = layers.Dense(64, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)
    x_blind = layers.Dense(64, activation='relu')(x_blind)

    x = layers.multiply([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_average(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim,
                                            maxlen):
    """prepare_cheap_multi_modal_model_average"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.average([x_blind, x_deaf])

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cheap_multi_modal_model_dot(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim,
                                        maxlen):
    """prepare_cheap_multi_modal_model_dot"""
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.dot([x_blind, x_deaf], axes=-1)

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)
    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_generic_keras_model(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen):
    """Generic Keras Model"""
    '''
    https://towardsdatascience.com/deep-learning-and-visual-question-answering-c8c8093941bc
    '''
    vision_model = Sequential()
    vision_model.add(layers.Conv2D(64, (3, 3), activation='relu', padding='same', input_shape=image_input_shape))
    vision_model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    vision_model.add(layers.MaxPooling2D((2, 2)))
    vision_model.add(layers.Conv2D(128, (3, 3), activation='relu', padding='same'))
    vision_model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    vision_model.add(layers.MaxPooling2D((2, 2)))
    vision_model.add(layers.Conv2D(256, (3, 3), activation='relu', padding='same'))
    vision_model.add(layers.Conv2D(256, (3, 3), activation='relu'))
    vision_model.add(layers.Conv2D(256, (3, 3), activation='relu'))
    vision_model.add(layers.MaxPooling2D((2, 2)))
    vision_model.add(layers.Flatten())

    image_input = Input(shape=image_input_shape)
    encoded_image = vision_model(image_input)

    # Define RNN for language input
    # question_input = Input(shape=(X_train['questions'][0].shape), dtype='int32')
    question_input = Input(shape=text_input_shape)
    embedded_question = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(question_input)

    encoded_question = layers.LSTM(256)(embedded_question)

    # Combine CNN and RNN to create the final model
    merged = layers.concatenate([encoded_question, encoded_image])
    output = layers.Dense(num_outputs, activation='softmax')(merged)
    model = Model(inputs=[question_input, image_input], outputs=output)
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def prepare_easy_vqa_model(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen):
    """Easy VQA keras Model"""
    '''
    https://victorzhou.com/blog/easy-vqa/
    '''
    im_input = Input(shape=image_input_shape)
    x1 = layers.Conv2D(8, 3, padding='same')(im_input)
    x1 = layers.MaxPooling2D()(x1)
    x1 = layers.Conv2D(16, 3, padding='same')(x1)
    x1 = layers.MaxPooling2D()(x1)
    big_model = True
    if big_model:
        x1 = layers.Conv2D(32, 3, padding='same')(x1)
        x1 = layers.MaxPooling2D()(x1)
    x1 = layers.Flatten()(x1)
    x1 = layers.Dense(32, activation='tanh', name='x1_before_mult')(x1)

    # The question network
    q_input = Input(shape=text_input_shape)
    x2 = layers.Dense(32, activation='tanh')(q_input)
    x2 = layers.Dense(32, activation='tanh', name='x2_before_mult')(x2)

    # Merge -> output
    out = layers.Multiply()([x1, x2])
    out = layers.Dense(32, activation='tanh')(out)
    out = layers.Dense(num_outputs, activation='softmax')(out)

    model = Model(inputs=[q_input, im_input], outputs=out)
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_basic_multimodal_model(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen):
    """Basic CNN-CNN model"""
    # input_tensor_blind = Input(shape=(59,))
    # input_tensor_deaf = Input(shape=(224, 224, 3))
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(64, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(64, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(128, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])
    # x = layers.multiply([x_blind, x_deaf])

    # We stack a deep densely-connected network on top
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)

    # And finally we add the main logistic regression layer
    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)

    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model


def prepare_advanced_resnet_multimodal_model(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                             embedding_dim, maxlen):
    """prepare_advanced_resnet_multimodal_model"""
    # input_tensor_blind = Input(shape=(59,))
    # input_tensor_deaf = Input(shape=(224, 224, 3))
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    conv_base = ResNet50(include_top=False)

    conv_base.trainable = True
    set_trainable = False
    for layer in conv_base.layers:
        if layer.name == 'block3_conv1':
            set_trainable = True
        if set_trainable:
            layer.trainable = True
        else:
            layer.trainable = False

    x_deaf = conv_base(input_tensor_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(256, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])

    # We stack a deep densely-connected network on top
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)

    # And finally we add the main logistic regression layer
    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)

    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())

    return model


def prepare_bio_trained_resnet_multimodal_model(image_input_shape, text_input_shape, num_outputs, vocab_size,
                                                embedding_dim, maxlen):
    """prepare_bio_trained_resnet_multimodal_model SINGLE LOSS"""
    # input_tensor_blind = Input(shape=(59,))
    # input_tensor_deaf = Input(shape=(224, 224, 3))
    input_tensor_blind = Input(shape=text_input_shape)
    input_tensor_deaf = Input(shape=image_input_shape)

    modality_p = data_dir + os.sep + 'medpix' + os.sep + 'trained_models' + os.sep + 'base_resnet_single_modality_loss.h5'

    # load model
    from keras.models import load_model
    bio_trained_model = load_model(modality_p)
    conv_base = [l for l in bio_trained_model.layers if l.name == 'resnet50'][0]
    conv_base.trainable = False

    x_deaf = conv_base(input_tensor_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(256, activation='relu')(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(128, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])

    # We stack a deep densely-connected network on top
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)
    x = layers.Dense(64, activation='relu')(x)

    # And finally we add the main logistic regression layer
    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(x)

    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())

    return model
