import numpy as np
from keras import Sequential, layers, Model
from keras import backend as K

from constants import data_dir
import os

blind_models_dir = data_dir + os.sep + 'blind_model'
glove_dir = data_dir + os.sep + "vocabs_and_utils" + os.sep + "glove.6B.300d.txt"

# from ktrain import EarlyStopping
optimizer = 'adam' # adamc

def prepare_dense_model(vocab_size, embedding_dim, maxlen, num_outputs):
    """Dense Model"""
    model = Sequential()
    model.add(layers.Embedding(input_dim=vocab_size,
                               output_dim=embedding_dim,
                               input_length=maxlen))
    model.add(layers.Flatten())
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model


def prepare_conv1d_model(vocab_size, embedding_dim, maxlen, num_outputs):
    """CNN Conv1D Model"""
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(128, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    # print(model.summary())
    return model


def prepare_cnn_model_no_predense(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_no_predense"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(128, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(f'prepare_cnn_model')
    # print(model.summary())
    return model

def prepare_cnn_model_double_conv(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_double_conv"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(128, 5, activation='relu'))
    model.add(layers.Conv1D(64, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(f'prepare_cnn_model_double_conv')
    # print(model.summary())
    return model

def prepare_cnn_model_double_conv_smaller_to_bigger(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_double_conv_smaller_to_bigger"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(32, 5, activation='relu'))
    model.add(layers.Conv1D(64, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(f'prepare_cnn_model_double_conv_smaller_to_bigger')
    print(model.summary())
    return model

def prepare_cnn_model_triple_conv_smaller_to_bigger(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_triple_conv_smaller_to_bigger"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(32, 5, activation='relu'))
    model.add(layers.Conv1D(64, 5, activation='relu'))
    model.add(layers.Conv1D(128, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(f'prepare_cnn_model_triple_conv_smaller_to_bigger')
    # print(model.summary())
    return model



def prepare_cnn_model_bigger_conv1d(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_bigger_conv1d"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(128, 7, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model

def prepare_cnn_model_bigger_pre_dense(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_bigger_pre_dense"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(128, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model

def prepare_cnn_model_smaller(vocab_size, embedding_dim, maxlen, num_outputs):
    """prepare_cnn_model_smaller"""
    print('conv1d params')
    print(vocab_size, embedding_dim, maxlen, num_outputs)
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.Conv1D(64, 5, activation='relu'))
    model.add(layers.GlobalMaxPooling1D())
    model.add(layers.Dense(10, activation='relu'))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model

def prepare_lstm_model(vocab_size, embedding_dim, maxlen, num_outputs):
    """LSTM Model"""
    model = Sequential()
    model.add(layers.Embedding(vocab_size, embedding_dim, input_length=maxlen))
    model.add(layers.SpatialDropout1D(0.2))
    model.add(layers.LSTM(20, dropout=0.2, recurrent_dropout=0.2))
    model.add(layers.Dense(num_outputs, activation='softmax'))
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model

def prepare_keras_lstm_attention_model(vocab_size, embedding_dim, maxlen, num_outputs):
    """Keras LSTM Attention"""
    input_tensor = layers.Input(shape=[maxlen], dtype='int32')

    units = 20
    # get the embedding layer
    embedded = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor)
    activations = layers.LSTM(units, return_sequences=True)(embedded)

    # compute importance for each step
    attention = layers.Dense(1, activation='tanh')(activations)
    attention = layers.Flatten()(attention)
    attention = layers.Activation('softmax')(attention)
    attention = layers.RepeatVector(units)(attention)
    attention = layers.Permute([2, 1])(attention)

    # sent_representation = merge([activations, attention], mode='mul')
    sent_representation = layers.multiply([activations, attention])
    sent_representation = layers.Lambda(lambda xin: K.sum(xin, axis=-2), output_shape=(units,))(sent_representation)
    probabilities = layers.Dense(num_outputs, activation='softmax')(sent_representation)
    model = Model(inputs=input_tensor, outputs=probabilities)
    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model


# def prepare_dense_model_glove(vocab_size, embedding_dim, maxlen, num_outputs):
#     embedding_data = get_glove_embedding(tokenizer, embedding_dim)
#     model = Sequential()
#     model.add(layers.Embedding(vocab_size,
#                                embedding_dim,
#                                weights=[embedding_data['matrix']],
#                                input_length=maxlen,
#                                trainable=False))
#     model.add(layers.Flatten())
#     model.add(layers.Dense(32, activation='relu'))
#     model.add(layers.Dense(32, activation='relu'))
#     model.add(layers.Dense(32, activation='relu'))
#     model.add(layers.Dense(num_outputs, activation='softmax'))
#     model.compile(optimizer='adam',
#                   loss='categorical_crossentropy',
#                   metrics=['accuracy'])
#     return model

# def get_glove_embedding(vocab_size, maxlen, num_outputs):
#     # credit: https://realpython.com/python-keras-text-classification/
#     word_index = tokenizer.word_index
#     vocab_size = len(word_index) + 1  # Adding again 1 because of reserved 0 index
#
#     embedding_matrix = np.zeros((vocab_size, embedding_dim))
#
#     with open(glove_dir, encoding='utf-8') as f:
#         for line in f:
#             word, *vector = line.split()
#             if word in word_index:
#                 idx = word_index[word]
#                 embedding_matrix[idx] = np.array(
#                     vector, dtype=np.float32)[:embedding_dim]
#
#     nonzero_elements = np.count_nonzero(np.count_nonzero(embedding_matrix, axis=1))
#     cover_percentage = nonzero_elements / vocab_size
#     print(f"embedding_matrix shape: {embedding_matrix.shape}, cover_percentage: {round(cover_percentage * 100, 2)}%")
#
#     embedding_data = {'name': 'glove', 'matrix': embedding_matrix}
#     return embedding_data