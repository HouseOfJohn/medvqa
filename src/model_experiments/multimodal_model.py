from distutils.dir_util import copy_tree

import cv2
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences

from constants import categories_names, dataset_type_to_dir_map
from src.Experiment import Experiment
from src.model_experiments.models_constructors.multimodal_models_constructors import *
from src.utils import get_data_dataframes, prepare_labels_to_fit, get_y_pred_and_fixed_y_test, \
    get_metrics_for_pred_and_truth, export_results, add_averages_to_scores_df, get_X_images, plot_history

multimodal_models_dir = data_dir + os.sep + 'model_experiments' + os.sep + 'multimodal_model'
DEBUG = False
EPOCHS = 20 if not DEBUG else 3
TOKENIZER_NUM_WORDS = 100
EMBEDDING_DIM = 25

print(f'DEBUG: {DEBUG}, EPOCHS: {EPOCHS}')

os.environ["CUDA_VISIBLE_DEVICES"] = "7"

experiment = Experiment(name="Multi Modal Model", export_output=True, deep_learning=True)


def main():
    df_all, df_train, df_valid, df_test = get_data_dataframes()

    models_constructors_funcs = [prepare_cheap_multi_modal_model, prepare_cheap_multi_modal_model_multiply,
                                 prepare_cheap_multi_modal_model_multiply_batch_norm_after_mult,
                                 prepare_generic_keras_model, prepare_easy_vqa_model, prepare_basic_multimodal_model,
                                 prepare_advanced_resnet_multimodal_model]

    # models_constructors_funcs = [prepare_easy_vqa_model]

    scores_dict = {}

    for model_constructor in models_constructors_funcs:
        model_doc = model_constructor.__doc__
        experiment.runtime_logger.log_task_start(model_doc)
        model_scores_df = get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test)
        print(model_doc)
        print(model_scores_df)
        scores_dict[model_doc] = model_scores_df
        experiment.runtime_logger.log_task_end(model_doc)

    export_results(experiment.output_dir, scores_dict)

    print("Done")


def get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test):
    model_scores_df = pd.DataFrame()
    for cat in categories_names:
        df_train_cat, df_valid_cat, df_test_cat, X_train, X_valid, X_test, \
        y_train_orig, y_valid_orig, y_test_orig, vocab_size, embedding_dim, maxlen = get_train_valid_test_data(cat, df_test, df_train, df_valid)
        y_train_dummies, y_valid_dummies = prepare_labels_to_fit(y_train_orig, y_valid_orig)
        cat_scores_series, y_pred, y_test_fixed = evaluate_multimodal_model(model_constructor, X_train, X_valid, X_test, cat, df_train_cat,
                                                      y_test_orig, y_train_dummies, y_valid_dummies, vocab_size,
                                                      embedding_dim, maxlen)

        # df_test_cat['y_pred'] = y_pred
        # df_test_cat['y_test_fixed'] = y_test_fixed
        # df_test_misclassification = df_test_cat.query('y_pred != answer')
        # print(f"df_test_misclassification: {len(df_test_misclassification)}")
        # print(df_test_misclassification[['answer', 'y_pred']])

        model_scores_df = model_scores_df.append(cat_scores_series)

    model_scores_df = add_averages_to_scores_df(model_scores_df)

    return model_scores_df



def evaluate_multimodal_model(model_constructor, X_train, X_valid, X_test, cat, df_train_cat, y_test_orig,
                              y_train_dummies, y_valid_dummies, vocab_size, embedding_dim, maxlen):
    image_input_shape = X_train['images'][0].shape
    text_input_shape = X_train['questions'][0].shape
    num_outputs = len(set(y_train_dummies.columns))
    model = model_constructor(image_input_shape, text_input_shape, num_outputs, vocab_size, embedding_dim, maxlen)
    history = model.fit([X_train['questions'], X_train['images']], y_train_dummies, epochs=EPOCHS, batch_size=32,
                        verbose=False,
                        validation_data=([X_valid['questions'], X_valid['images']], y_valid_dummies),
                        callbacks=[EarlyStopping(monitor='val_loss', mode='min')])
    # history = model.fit([X_train['questions'], X_train['images']], y_train_dummies, epochs=EPOCHS, batch_size=32,
    #                     verbose=False,
    #                     validation_data=([X_valid['questions'], X_valid['images']], y_valid_dummies))
    plot_history(experiment.dl_plots, history, model_constructor.__doc__ + " " + cat)
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test([X_test['questions'], X_test['images']], model, y_test_orig, y_train_dummies)
    acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
    ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())
    return ser, y_pred, y_test_fixed


def get_train_valid_test_data(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['category'] == cat]
    df_valid_cat = df_valid[df_valid['category'] == cat]
    df_test_cat = df_test[df_test['category'] == cat]

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    X_train_images = get_X_images(df_train_cat, dataset_type='train')
    X_valid_images = get_X_images(df_valid_cat, dataset_type='valid')
    X_test_images = get_X_images(df_test_cat, dataset_type='test')

    X_train_questions_orig, X_valid_questions_orig, X_test_questions_orig = df_train_cat['question'].values, \
                                                                            df_valid_cat['question'].values, \
                                                                            df_test_cat['question'].values
    maxlen = get_maximum_question_length(cat, df_train_cat, df_test_cat)
    tokenizer = Tokenizer(num_words=TOKENIZER_NUM_WORDS)
    tokenizer.fit_on_texts(X_train_questions_orig)
    X_train_questions = tokenizer.texts_to_sequences(X_train_questions_orig)
    X_valid_questions = tokenizer.texts_to_sequences(X_valid_questions_orig)
    X_test_questions = tokenizer.texts_to_sequences(X_test_questions_orig)
    vocab_size = len(tokenizer.word_index) + 1  # Adding 1 because of reserved 0 index
    X_train_questions = pad_sequences(X_train_questions, padding='post', maxlen=maxlen)
    X_valid_questions = pad_sequences(X_valid_questions, padding='post', maxlen=maxlen)
    X_test_questions = pad_sequences(X_test_questions, padding='post', maxlen=maxlen)
    embedding_dim = EMBEDDING_DIM

    y_train_orig, y_valid_orig, y_test_orig = df_train_cat['answer'].values, df_valid_cat['answer'].values, df_test_cat[
        'answer'].values

    X_train = {'questions': X_train_questions, 'images': X_train_images}
    X_valid = {'questions': X_valid_questions, 'images': X_valid_images}
    X_test = {'questions': X_test_questions, 'images': X_test_images}

    return df_train_cat, df_valid_cat, df_test_cat, X_train, X_valid, X_test, y_train_orig, y_valid_orig, y_test_orig, vocab_size, embedding_dim, maxlen

def get_maximum_question_length(cat, df_train_cat, df_valid_cat):
    all_questions = list(df_train_cat['question'].values) + list(df_valid_cat['question'])
    all_questions_len = [len(w) for w in all_questions]
    maxlen = max(all_questions_len)
    return maxlen



if __name__ == '__main__':
    main()
    copy_tree(experiment.output_dir, os.path.join(multimodal_models_dir, 'outputs'))
    copy_tree(experiment.dl_plots, os.path.join(multimodal_models_dir, 'plots'))
    experiment.end()
