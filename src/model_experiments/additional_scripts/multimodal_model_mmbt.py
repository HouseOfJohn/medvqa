import os

import pandas as pd
import sklearn
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, f1_score

from constants import dataset_type_to_dir_map, categories_names
from src.utils import get_data_dataframes, fix_y_test

DEBUG = False
os.environ["CUDA_VISIBLE_DEVICES"] = "3"

print(f'DEBUG: {DEBUG}')

def main():
    all_df, train_df, valid_df, test_df = get_data_dataframes()
    scores_df = pd.DataFrame()

    for cat in categories_names:
        print(f"Category - {cat}")

        ser = eval_mmbt_model(cat, test_df, train_df, valid_df)
        scores_df = scores_df.append(ser)

        print(f"Finished with category {cat}\n\n\n")

    print(scores_df)
    print("Done")


def eval_mmbt_model(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['category'] == cat][['question', 'answer', 'pic_path']]
    df_valid_cat = df_valid[df_valid['category'] == cat][['question', 'answer', 'pic_path']]
    df_test_cat = df_test[df_test['category'] == cat][['question', 'answer', 'pic_path']]

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    df_train_cat.rename(columns={"question": "text", "answer": "labels", 'pic_path': 'images'}, inplace=True)
    df_valid_cat.rename(columns={"question": "text", "answer": "labels", 'pic_path': 'images'}, inplace=True)
    df_test_cat.rename(columns={"question": "text", "answer": "labels", 'pic_path': 'images'}, inplace=True)

    le = preprocessing.LabelEncoder()

    all_answers = list(set(list(df_train_cat['labels'].values) + list(df_valid_cat['labels'].values)))
    num_labels = len(all_answers)
    le.fit(all_answers)
    print(f"Number of classes: {len(le.classes_)}")
    df_train_cat['labels'] = list(le.transform(df_train_cat['labels'].values))
    df_valid_cat['labels'] = list(le.transform(df_valid_cat['labels'].values))

    from simpletransformers.classification.multi_modal_classification_model import MultiModalClassificationModel

    model = MultiModalClassificationModel("bert", "bert-base-uncased", num_labels=num_labels, use_cuda=True,
                                          args={'reprocess_input_data': True, 'overwrite_output_dir': True})

    images_path = dataset_type_to_dir_map['train'] + os.sep + 'images'
    model.train_model(df_train_cat, image_path=images_path, show_running_loss=False)

    print(f"after train_model")

    result, model_outputs = model.eval_model(df_valid_cat, image_path=dataset_type_to_dir_map['valid'] + os.sep + 'images', accuracy=sklearn.metrics.accuracy_score)
    print(f"result: {result}")

    df_test_cat_without_labels = df_test_cat[['text', 'images']]
    df_test_dict = {'text': list(df_test_cat_without_labels['text'].values), 'images': list(df_test_cat_without_labels['images'].values)}
    predictions, raw_outputs = model.predict(df_test_dict, image_path=dataset_type_to_dir_map['test'] + os.sep + 'images')

    print(f"After predict")
    print(predictions)

    y_pred = le.inverse_transform(predictions)
    y_test = df_test_cat['labels'].values

    y_test_fixed = fix_y_test(y_test, y_pred)
    acc = accuracy_score(y_test_fixed, y_pred)
    f1_score_val = f1_score(y_test_fixed, y_pred, average='weighted')
    ser = pd.Series({'accuracy': acc, 'weighted f1_score': f1_score_val}, name=cat + " Keras CNN")
    print(ser)
    return ser



if __name__ == '__main__':
    main()