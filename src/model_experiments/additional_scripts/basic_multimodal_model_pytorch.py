import os

import cv2
import numpy as np
import pandas as pd
pd.set_option('display.max_columns', None)
import torch
from keras import layers, Input, Model
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from nltk.translate.bleu_score import corpus_bleu
from sklearn.metrics import accuracy_score, f1_score
from torch import nn
from torch.utils.data import TensorDataset, DataLoader

from constants import data_dir, categories_names_without_abnormality, dataset_type_to_dir_map, categories_names_map
from src.model_experiments.deaf_model import get_X_images
from src.models_utils import prepare_labels_to_fit, fix_y_test, plot_history

DEBUG = False
EPOCHS = 20 if not DEBUG else 1

print(f'DEBUG: {DEBUG}')

def main():
    all_df, train_df, valid_df, test_df = get_data_dataframes()
    evaluate_multimodal_model(train_df, valid_df, test_df)

def get_data_dataframes(to_csv=False):
    df_train = read_qa_pairs(dataset_type='train', images=False)
    df_valid = read_qa_pairs(dataset_type='valid', images=False)
    df_test = read_qa_pairs(dataset_type='test', images=False)

    all_df = pd.concat([df_train, df_valid, df_test], axis=0, sort=False)
    print(f"merged data at len: {len(all_df)}")

    if to_csv:
        df_train.to_csv(data_dir + os.sep + "train.csv", index=False)
        df_valid.to_csv(data_dir + os.sep + "valid.csv", index=False)
        df_test.to_csv(data_dir + os.sep + "test.csv", index=False)
        print(f"Wrote to_csv {data_dir}")

    return all_df, df_train, df_valid, df_test


def get_train_valid_test_data(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['question_type'] == cat]
    df_valid_cat = df_valid[df_valid['question_type'] == cat]
    df_test_cat = df_test[df_test['question_type'] == cat]

    X_train_images = get_X_images(df_train_cat, dataset_type='train')
    X_valid_images = get_X_images(df_valid_cat, dataset_type='valid')
    X_test_images = get_X_images(df_test_cat, dataset_type='test')

    X_train_questions_orig, X_valid_questions_orig, X_test_questions_orig = df_train_cat['question'].values, df_valid_cat['question'].values, \
                                              df_test_cat['question'].values
    maxlen = get_maximum_question_length(cat, df_train_cat, df_test_cat)
    tokenizer = Tokenizer(num_words=100)
    tokenizer.fit_on_texts(X_train_questions_orig)
    X_train_questions = tokenizer.texts_to_sequences(X_train_questions_orig)
    X_valid_questions = tokenizer.texts_to_sequences(X_valid_questions_orig)
    X_test_questions = tokenizer.texts_to_sequences(X_test_questions_orig)
    vocab_size = len(tokenizer.word_index) + 1  # Adding 1 because of reserved 0 index
    X_train_questions = pad_sequences(X_train_questions, padding='post', maxlen=maxlen)
    X_valid_questions = pad_sequences(X_valid_questions, padding='post', maxlen=maxlen)
    X_test_questions = pad_sequences(X_test_questions, padding='post', maxlen=maxlen)
    embedding_dim = 25

    y_train_orig, y_valid_orig, y_test_orig = df_train_cat['answer'].values, df_valid_cat['answer'].values, df_test_cat['answer'].values

    X_train = {'questions': X_train_questions, 'images': X_train_images}
    X_valid = {'questions': X_valid_questions, 'images': X_valid_images}
    X_test = {'questions': X_test_questions, 'images': X_test_images}

    X_train_val = {'questions': np.concatenate((X_train_questions, X_valid_questions)), 'images': np.concatenate((X_train_images, X_valid_images))}
    y_train_val_orig = np.concatenate((y_train_orig, y_valid_orig))

    return X_train_val, y_train_val_orig, X_test, y_test_orig, vocab_size, embedding_dim, maxlen


def evaluate_multimodal_model(df_train, df_valid, df_test):

    scores_df = pd.DataFrame()

    for cat in categories_names_without_abnormality:
        print(f"Category - {cat}")

        X_train_val, y_train_val_orig, X_test, y_test_orig, vocab_size, embedding_dim, maxlen\
            = get_train_valid_test_data(cat, df_test, df_train, df_valid)

        y_train_val_dummies = prepare_labels_to_fit(y_train_val_orig)
        num_answers = len(set(y_train_val_orig))

        scores_df = evaluate_basic_model_pytorch(prepare_cheap_multimodal_model_original, X_train_val, X_test, cat,
                                                scores_df, y_test_orig, y_train_val_orig, y_train_val_dummies, vocab_size,
                                                embedding_dim, maxlen, num_answers)

        print(f"Finished with category {cat}\n\n\n")

    print(scores_df)
    print("Done")

def prepare_labels_to_fit(y_train_val_orig):
    y_train_dummies = pd.get_dummies(y_train_val_orig)
    y_train_dummies.fillna(0, inplace=True)
    return y_train_dummies


def prepare_cheap_multimodal_model_original(X_train, df_train_cat, vocab_size, embedding_dim, maxlen):
    """Cheap multimodal_model original"""
    '''
    modality Cheap multimodal_model     0.640           0.622065
    plane Cheap multimodal_model        0.640           0.597468
    organ Cheap multimodal_model        0.664           0.635159
    '''
    input_tensor_blind = Input(shape=X_train['questions'][0].shape)
    input_tensor_deaf = Input(shape=X_train['images'][0].shape)

    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)

    x_blind = layers.Embedding(vocab_size, embedding_dim, input_length=maxlen)(input_tensor_blind)
    x_blind = layers.Conv1D(20, 5, activation='relu')(x_blind)
    x_blind = layers.GlobalMaxPooling1D()(x_blind)

    x = layers.concatenate([x_blind, x_deaf])

    x = layers.BatchNormalization()(x)
    x = layers.Dense(10, activation='relu')(x)

    # And finally we add the main logistic regression layer
    main_output = layers.Dense(len(set(df_train_cat['answer'].values)), activation='softmax', name='main_output')(x)

    model = Model(inputs=[input_tensor_blind, input_tensor_deaf], outputs=main_output)

    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model

class ImagesModel(nn.Module):
    def __init__(self):
        super(ImagesModel, self).__init__()

        self.conv_pool_layers = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3),
            nn.Conv2d(32, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3),
            nn.Conv2d(32, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3),
            nn.Conv2d(32, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3),
            # nn.Conv2d(32, 32, kernel_size=3),
            # nn.ReLU(inplace=True),
            # nn.MaxPool2d(kernel_size=3),
        )

    def forward(self, x):
        permuted_x = x.permute(0, 3, 1, 2)

        permuted_x_conved = self.conv_pool_layers(permuted_x)

        image_features = torch.flatten(permuted_x_conved, start_dim=1)
        return image_features

class QuestionsModel(nn.Module):
    def __init__(self, vocab_size, embedding_dim, maxlen):
        super(QuestionsModel, self).__init__()
        self.embedding = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embedding_dim)
        self.conv_1d = nn.Conv1d(in_channels=embedding_dim, out_channels=32, kernel_size=5)
        # x_blind = layers.Conv1D(20, 5, activation='relu')(x_blind)
        self.relu = nn.ReLU()

        self.global_max_pool = nn.AdaptiveAvgPool1d(1)

    def forward(self, x):
        x_emb = self.embedding(x)

        x_emb_permuted = x_emb.permute(0, 2, 1)

        x_emb_conved = self.conv_1d(x_emb_permuted)
        x_emb_conved_relued = self.relu(x_emb_conved)
        question_feats = self.global_max_pool(x_emb_conved_relued)
        question_feats = torch.flatten(question_feats, start_dim=1)
        return question_feats

class MultiModalModel(nn.Module):
    def __init__(self, num_answers, vocab_size, embedding_dim, maxlen):
        super(MultiModalModel, self).__init__()

        self.images_model = ImagesModel()
        self.questions_model = QuestionsModel(vocab_size, embedding_dim, maxlen)
        self.batch_norm = nn.BatchNorm1d(64)
        self.num_answers = num_answers

        self.dense1 = nn.Linear(64, 10)
        self.relu = nn.ReLU()

        self.dense2 = nn.Linear(10, self.num_answers)
        self.softmax = nn.Softmax()


    def forward(self, x_images, x_questions):

        x_images = self.images_model(x_images)
        x_questions = self.questions_model(x_questions)

        x_cat = torch.cat((x_images, x_questions), axis=1)
        x_cat_normed = self.batch_norm(x_cat)

        x = self.dense1(x_cat_normed)
        x = self.relu(x)

        x = self.dense2(x)
        x = self.softmax(x)

        return x

def evaluate_basic_model(model_func, X_train, X_valid, X_test, cat, df_train_cat, scores_df, y_test_orig,
                         y_train_dummies, y_valid_dummies, vocab_size, embedding_dim, maxlen):
    model = model_func(X_train, df_train_cat, vocab_size, embedding_dim, maxlen)
    history = model.fit([X_train['questions'], X_train['images']], y_train_dummies, epochs=EPOCHS, batch_size=32,
                        verbose=False,
                        validation_data=([X_valid['questions'], X_valid['images']], y_valid_dummies))
    loss, accuracy = model.evaluate([X_train['questions'], X_train['images']], y_train_dummies, verbose=False)
    plot_history(history, cat + " " + "pytorch_" + model_func.__doc__)
    print("Training Accuracy: {:.4f}".format(accuracy))
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test([X_test['questions'], X_test['images']], model, y_test_orig, y_train_dummies)
    acc = accuracy_score(y_test_fixed, y_pred)
    f1_score_val = f1_score(y_test_fixed, y_pred, average='weighted')
    ser = pd.Series({'accuracy': acc, 'weighted f1_score': f1_score_val}, name=cat + " " + model_func.__doc__)
    print(ser)
    scores_df = scores_df.append(ser)
    return scores_df

def compute_accuracy(y_pred, y_target):
    y_target = y_target.cpu()
    y_pred_indices = (torch.sigmoid(y_pred)>0.5).cpu().long()#.max(dim=1)[1]
    n_correct = torch.eq(y_pred_indices, y_target).sum().item()
    return n_correct / len(y_pred_indices) * 100

def evaluate_basic_model_pytorch(model_func, X_train_val, X_test, cat, scores_df, y_test_orig, y_train_val_orig, y_train_val_dummies,
                                 vocab_size, embedding_dim, maxlen, num_answers):

    model = MultiModalModel(num_answers, vocab_size, embedding_dim, maxlen)
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    train_loader = prepare_loader(X_train_val, device, y_train_val_dummies)
    test_loader = prepare_loader(X_test, device, y_dummies=None, test=True)

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    train_state = make_train_state()

    for epoch_index in range(EPOCHS):
        model.train()
        train_epoch(criterion, epoch_index, train_loader, model, optimizer, train_state)

    model.eval()

    y_pred_probas = []

    for batch_index, (questions_batch, images_batch) in enumerate(test_loader):
        with torch.no_grad():
            batch_y_pred_probas = model(x_questions=questions_batch, x_images=images_batch)
            y_pred_probas += list(batch_y_pred_probas.numpy())

    y_pred_classes = np.argmax(y_pred_probas,axis=1)
    y_pred = [y_train_val_dummies.columns[pred_idx] for pred_idx in y_pred_classes]
    y_test_fixed = fix_y_test(y_test_orig, y_pred)

    acc = accuracy_score(y_test_fixed, y_pred)
    f1_score_val = f1_score(y_test_fixed, y_pred, average='weighted')

    bleu_score = corpus_bleu([[x] for x in y_test_fixed], y_pred)

    ser = pd.Series({'accuracy': acc, 'weighted f1_score': f1_score_val, 'bleu_score': bleu_score}, name=cat + " " + model_func.__doc__)
    print(ser)
    scores_df = scores_df.append(ser)
    return scores_df


def train_batch(batch_index, criterion, images_batch, model, optimizer, questions_batch, running_acc, running_loss,
                targets_batch):
    optimizer.zero_grad()
    # step 2. compute the output
    y_pred = model(x_questions=questions_batch, x_images=images_batch)
    # step 3. compute the loss
    batch_target_indices = torch.max(targets_batch, 1)[1]
    loss = criterion(y_pred, batch_target_indices)
    loss_t = loss.item()
    running_loss += (loss_t - running_loss) / (batch_index + 1)
    # step 4. use loss to produce gradients
    loss.backward()
    # step 5. use optimizer to take gradient step
    optimizer.step()
    # -----------------------------------------
    # compute the accuracy
    acc_t = compute_accuracy(y_pred, targets_batch)
    running_acc += (acc_t - running_acc) / (batch_index + 1)
    return running_acc, running_loss


def prepare_loader(X, device, y_dummies, test=False):
    train_question_tensors = torch.from_numpy(X['questions']).long().to(device)
    train_images_tensors = torch.from_numpy(X['images']).float().to(device)
    if not test:
        train_targets_tensors = torch.from_numpy(y_dummies.values)
        dataset = TensorDataset(train_question_tensors, train_images_tensors, train_targets_tensors)
    else:
        dataset = TensorDataset(train_question_tensors, train_images_tensors)
    train_loaded = DataLoader(
        dataset,
        batch_size=32
    )
    return train_loaded


def train_epoch(criterion, epoch_index, loader, model, optimizer, train_state):
    train_state['epoch_index'] = epoch_index
    running_loss = 0.0
    running_acc = 0.0
    for batch_index, (questions_batch, images_batch, targets_batch) in enumerate(loader):
        running_acc, running_loss = train_batch(batch_index, criterion, images_batch, model, optimizer,
                                                questions_batch, running_acc, running_loss, targets_batch)
    print(f"epoch: {epoch_index}, train_loss: {running_loss}, train_acc: {running_acc}")
    train_state['train_loss'].append(running_loss)
    train_state['train_acc'].append(running_acc)




def make_train_state():
    return {'stop_early': False,
            'early_stopping_step': 0,
            'early_stopping_best_val': 1e8,
            'epoch_index': 0,
            'train_loss': [],
            'train_acc': [],
            'val_loss': [],
            'val_acc': [],
            'test_loss': -1,
            'test_acc': -1}

def get_train_valid_answers(df_train, df_valid):
    train_answers = list(df_train['answer'].values)
    valid_answers = list(df_valid['answer'].values)
    all_answers_lst = train_answers + valid_answers
    return all_answers_lst


def get_maximum_question_length(cat, df_train_cat, df_valid_cat):
    all_questions = list(df_train_cat['question'].values) + list(df_valid_cat['question'])
    all_questions_len = [len(w) for w in all_questions]
    maxlen = max(all_questions_len)
    print(f"cat: {cat}, maxlen: {maxlen}")
    return maxlen


def load_image(infilename) :
    image = cv2.imread(infilename)
    resized = cv2.resize(image, (224, 224), interpolation=cv2.INTER_AREA)
    return resized





def read_qa_pairs(dataset_type, images=True):
    dir_p = dataset_type_to_dir_map[dataset_type]
    dir_pics_p = dir_p + os.sep + 'images'

    if dataset_type != 'test':
        if dataset_type == 'valid':
            dataset_type = 'val'
        df = pd.DataFrame()
        dir_qa_pic = dir_p + os.sep + 'QAPairsByCategory'
        for cat_key, cat_value, in categories_names_map.items():
            cat_p = dir_qa_pic + os.sep + cat_value + "_" + dataset_type + ".txt"
            with open(cat_p, encoding='utf-8') as f:
                lines = [x.rstrip('\n') for x in f.readlines()]
            lines = [get_line_parts(line, dir_pics_p) for line in lines]
            cat_df = pd.DataFrame(lines)
            cat_df['question_type'] = cat_key
            df = pd.concat([df, cat_df])
            # all_categories_lines[cat] = lines
    else:
        dir_p += os.sep + 'VQAMed2019_Test_Questions_w_Ref_Answers.txt'
        with open(dir_p) as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts(line, dir_pics_p, test=True) for line in lines]
        df = pd.DataFrame(lines)

    print(f"\nGot {dataset_type} dataframe at len: {len(df)}")
    print(df['question_type'].value_counts())

    df['dataset_type'] = dataset_type
    return df

def get_line_parts(line, pics_dir, test=False):
    if test:
        qa_id, question_type, question, answer = line.split("|")
        pic_p = qa_id + ".jpg"
        return {"qa_id": qa_id, "pic_path": pic_p, "question_type": question_type, "question": question, "answer": answer}
    else:
        qa_id, question, answer = line.split("|")
        pic_p = qa_id + ".jpg"
        return {"qa_id": qa_id, "pic_path": pic_p, "question": question, "answer": answer}


def read_data(dataset_dtype):
    data_p = data_dir + os.sep + dataset_dtype + ".csv"
    df = pd.read_csv(data_p)
    return df


def get_y_pred_and_fixed_y_test(X_test, model, y_test_orig, y_train_dummies):
    y_pred_probas = model.predict(X_test, verbose=False)
    y_pred_classes = np.argmax(y_pred_probas,axis=1)
    y_pred = [y_train_dummies.columns[pred_idx] for pred_idx in y_pred_classes]
    y_test_fixed = fix_y_test(y_test_orig, y_pred)
    return y_pred, y_test_fixed

if __name__ == '__main__':
    main()