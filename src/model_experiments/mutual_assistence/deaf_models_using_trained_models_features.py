import os
from distutils.dir_util import copy_tree

import cv2
import numpy as np
import pandas as pd
from keras import Input
from keras.engine.saving import load_model
from keras.utils import plot_model

from constants import categories_names, data_dir, dataset_type_to_dir_map, categories_names_without_abnormality
from src.Experiment import Experiment
from src.model_experiments.models_constructors.deaf_models_constructors import *
from src.utils import get_data_dataframes, prepare_labels_to_fit, get_y_pred_and_fixed_y_test, \
    get_metrics_for_pred_and_truth, export_results, add_averages_to_scores_df, get_X_images, plot_history

DEBUG = False
EPOCHS = 20 if not DEBUG else 3

print(f'DEBUG: {DEBUG}')
os.environ["CUDA_VISIBLE_DEVICES"] = "6"

last_dense_layer_size_for_helpers = 100
last_dense_layer_size_primary = 250

experiment = Experiment(name=f"Using trained models features - Primary-{last_dense_layer_size_primary}, *2* Helpers-{last_dense_layer_size_for_helpers}", export_output=True, deep_learning=True)

trained_deaf_models_dirs = os.path.join(data_dir, "experiments_logs", "trained_deaf_models", "outputs")

def main():
    df_all, df_train, df_valid, df_test = get_data_dataframes()

    scores_dict = {}

    model_constructor = prepare_cnn_model_exp_hirerchical
    model_doc = model_constructor.__doc__
    experiment.runtime_logger.log_task_start(model_doc)
    model_scores_df = get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test)
    print(model_doc)
    print(model_scores_df)
    scores_dict[model_doc] = model_scores_df
    experiment.runtime_logger.log_task_end(model_doc)

    export_results(experiment.output_dir, scores_dict)

    print("Done")


def get_categories_scores_for_model(model_constructor, df_train, df_valid, df_test):
    model_scores_df = pd.DataFrame()

    for cat in categories_names:
        df_train_cat, df_valid_cat, df_test_cat, X_train_orig, X_valid_orig, X_test_orig, y_train_orig, y_valid_orig, y_test_orig \
            = get_train_valid_test_data(cat, df_test, df_train, df_valid)
        y_train_dummies, y_valid_dummies = prepare_labels_to_fit(y_train_orig, y_valid_orig)
        cat_scores_series = evaluate_vision_model(model_constructor, X_test_orig, X_valid_orig, X_train_orig, cat,
                                                  df_train_cat, df_valid_cat, y_test_orig, y_train_dummies, y_valid_dummies)


        model_scores_df = model_scores_df.append(cat_scores_series)

    model_scores_df = add_averages_to_scores_df(model_scores_df)

    return model_scores_df


def create_model_with_trained_models(input_shape, num_outputs, category):
    # other_categories = set(categories_names).difference({category})
    other_categories = set(categories_names_without_abnormality).difference({category})

    input_tensor_deaf = Input(shape=input_shape)

    all_extracted_features = []

    for other_cat in other_categories:
        other_model_p = os.path.join(trained_deaf_models_dirs, f"trained_{category}_model.h5")
        other_model = load_model(other_model_p)
        new_other_model = Model(other_model.inputs, other_model.layers[-2].output, name=other_cat)
        print(f"Summary for {other_cat}")
        new_other_model.set_weights(other_model.get_weights())
        new_other_model.trainable = False
        other_model_features = new_other_model(input_tensor_deaf)
        print(f"Creating {other_cat}_dense")
        other_model_dense_output = layers.Dense(last_dense_layer_size_for_helpers, activation='relu', name=f"{other_cat}_dense")(other_model_features)
        all_extracted_features.append(other_model_dense_output)
        print(new_other_model.summary())
        print()

    primary_model_features = get_primary_model_features(input_tensor_deaf)

    all_extracted_features.append(primary_model_features)

    all_extracted_features = layers.concatenate(all_extracted_features)

    main_output = layers.Dense(num_outputs, activation='softmax', name='main_output')(all_extracted_features)
    model = Model(inputs=input_tensor_deaf, outputs=main_output)

    model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(model.summary())
    plot_model(model, to_file=os.path.join(experiment.output_dir, f'{category}_model_plot.png'))

    print("Done preparing model")
    return model

def get_primary_model_features(input_tensor_deaf):
    x_deaf = layers.Conv2D(4, (3, 3), activation='relu')(input_tensor_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(8, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(16, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(32, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Conv2D(48, (3, 3), activation='relu')(x_deaf)
    x_deaf = layers.MaxPooling2D((2, 2))(x_deaf)
    x_deaf = layers.Flatten()(x_deaf)
    x_deaf = layers.Dense(last_dense_layer_size_primary, activation='relu', name="primary_dense")(x_deaf)
    return x_deaf

def evaluate_vision_model(model_constructor, X_test_orig, X_valid_orig, X_train_orig, cat, df_train_cat, df_valid_cat, y_test_orig, y_train_dummies, y_valid_dummies):
    # model = model_constructor(input_shape=X_train_orig[0].shape, num_outputs=len(set(df_train_cat['answer'].values))) # Create other model.
    model = create_model_with_trained_models(input_shape=X_train_orig[0].shape, num_outputs=len(set(df_train_cat['answer'].values)), category=cat)
    history = model.fit(X_train_orig, y_train_dummies, epochs=EPOCHS, batch_size=32, verbose=False,
                        validation_data=(X_valid_orig, y_valid_dummies),
                        callbacks=[EarlyStopping(monitor='val_loss', mode='min')])
    plot_history(experiment.dl_plots, history, model_constructor.__doc__ + " " + cat)
    y_pred, y_test_fixed = get_y_pred_and_fixed_y_test(X_test_orig, model, y_test_orig, y_train_dummies)
    acc, bleu_score, f1_score_val = get_metrics_for_pred_and_truth(y_pred, y_test_fixed)
    ser = pd.Series({'Accuracy': acc, 'F1-score': f1_score_val, 'BLEU': bleu_score}, name=cat.capitalize())

    df_train_valid_cat = pd.concat([df_train_cat, df_valid_cat])
    X_train_valid_orig = np.concatenate([X_train_orig, X_valid_orig])
    y_train_valid_dummies = pd.concat([y_train_dummies, y_valid_dummies])

    return ser


def get_train_valid_test_data(cat, df_test, df_train, df_valid):
    df_train_cat = df_train[df_train['category'] == cat]
    df_valid_cat = df_valid[df_valid['category'] == cat]
    df_test_cat = df_test[df_test['category'] == cat]

    if DEBUG:
        df_train_cat = df_train_cat.head(20)
        df_valid_cat = df_valid_cat.head(20)
        df_test_cat = df_test_cat.head(20)

    X_train = get_X_images(df_train_cat, dataset_type='train')
    X_valid = get_X_images(df_valid_cat, dataset_type='valid')
    X_test = get_X_images(df_test_cat, dataset_type='test')

    y_train_orig, y_valid_orig, y_test_orig = df_train_cat['answer'].values, df_valid_cat['answer'].values, df_test_cat['answer'].values

    return df_train_cat, df_valid_cat, df_test_cat, X_train, X_valid, X_test, y_train_orig, y_valid_orig, y_test_orig


if __name__ == '__main__':
    main()
    experiment.end()
