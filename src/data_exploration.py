import operator
import os
from collections import defaultdict
from copy import deepcopy
from distutils.dir_util import copy_tree

import matplotlib.pyplot as plt
import pandas as pd
from nltk.translate.bleu_score import corpus_bleu
from simpletransformers.classification import ClassificationModel
from sklearn.metrics import accuracy_score, f1_score

from src.Experiment import Experiment
from constants import dataset_type_to_dir_map, categories_names_map, data_dir, \
    categories_names
from src.utils import fix_y_test


data_exploration_dir = data_dir + os.sep + 'data_exploration'

experiment = Experiment(name="Data Exploration", export_output=True)

def main():

    all_df, df_train, df_valid, df_test = get_data_dataframes_and_export_stats()

    get_number_of_words_stats(all_df, df_test, df_train, df_valid)

    check_bert_vocab_intersection(all_df)

    produce_answers_distribution_plots(all_df)

    predict_with_most_common_ans_per_category(df_test)

    in_test_but_not_in_train = check_correlation_with_test(df_train, df_valid, df_test)

    predict_with_most_common_ans_given_question(all_df, df_test, in_test_but_not_in_train)

    print("Done")


def get_number_of_words_stats(all_df, df_test, df_train, df_valid):
    num_words, num_distinct_words = get_words_number(all_df)
    num_words_train, num_distinct_words_train = get_words_number(df_train)
    num_words_valid, num_distinct_words_valid = get_words_number(df_valid)
    num_words_test, num_distinct_words_test = get_words_number(df_test)
    s_all = pd.Series({'Total': num_words, 'Distinct': num_distinct_words}, name='All')
    s_train = pd.Series({'Total': num_words_train, 'Distinct': num_distinct_words_train}, name='Train')
    s_valid = pd.Series({'Total': num_words_valid, 'Distinct': num_distinct_words_valid}, name='Valid')
    s_test = pd.Series({'Total': num_words_test, 'Distinct': num_distinct_words_test}, name='Test')
    number_of_words_stats = pd.DataFrame([s_all, s_train, s_valid, s_test])
    print("Number of words stats")
    print(number_of_words_stats)
    print()
    number_of_words_stats.to_excel(os.path.join(experiment.output_dir, "Number of words stats.xlsx"))


def get_bert_word_pieces_difference(in_vocab_but_not_in_bert, bert_base_vocab):
    model = ClassificationModel('bert', 'bert-base-cased', use_cuda=False)
    word_pieces_not_in_bert_vocab = defaultdict(list)
    for w in in_vocab_but_not_in_bert:
        tokenized_w_parts = model.tokenizer.tokenize(w)
        for tokenized_part in tokenized_w_parts:
            if tokenized_part not in bert_base_vocab:
                word_pieces_not_in_bert_vocab[w].append(tokenized_part)
    return word_pieces_not_in_bert_vocab


def check_bert_vocab_intersection(all_df):
    all_question_words_lst, all_question_words_set = get_all_question_words(all_df)

    with open(data_dir + os.sep + "vocabs_and_utils" + os.sep + "bert_base_vocab.txt", encoding='utf-8') as f:
        bert_base_vocab = set([x.rstrip('\n') for x in f.readlines()]) # 30K words

    with open(data_dir + os.sep + "vocabs_and_utils" + os.sep + "biobert_vocab.txt", encoding='utf-8') as f:
        biobert_vocab = set([x.rstrip('\n') for x in f.readlines()]) # 28K words

    print("Correlation with BERT and BioBERT")
    in_vocab_but_not_in_bert_vocab = all_question_words_set.difference(bert_base_vocab)
    in_vocab_but_not_in_bio_bert_vocab = all_question_words_set.difference(biobert_vocab)

    in_vocab_but_not_in_bert = get_bert_word_pieces_difference(in_vocab_but_not_in_bert_vocab, bert_base_vocab)
    in_vocab_but_not_in_bio_bert = get_bert_word_pieces_difference(in_vocab_but_not_in_bio_bert_vocab, biobert_vocab)

    corr_with_bert_and_bio_bert = {'Bert word-piece VOCAB': len(bert_base_vocab),
                                   'BioBert word-piece VOCAB': len(biobert_vocab),
                                   'Word-pieces in vocab but not in Bert': len(in_vocab_but_not_in_bert),
                                   'Word-pieces in vocab but not in Bio-Bert': len(in_vocab_but_not_in_bio_bert),
                                   'BERT & BioBERT word piece intersection': len(bert_base_vocab.intersection(biobert_vocab)),
                                  }
    corr_with_bert_and_bio_bert_series = pd.Series(corr_with_bert_and_bio_bert)
    print(corr_with_bert_and_bio_bert_series)
    corr_with_bert_and_bio_bert_series.to_excel(os.path.join(experiment.output_dir, "Correlation with BERT and BioBERT.xlsx"))
    if len(in_vocab_but_not_in_bert) > 0:
        print('Word-pieces in vocab but not in Bert')
        print(dict(in_vocab_but_not_in_bert))

    if len(in_vocab_but_not_in_bio_bert) > 0:
        print('Word-pieces in vocab but not in Bio-Bert')
        print(dict(in_vocab_but_not_in_bio_bert))

    print()


def get_words_number(df):
    ws1, ws2 = get_all_question_words(df)
    return len(ws1), len(ws2)

def get_all_question_words(df):
    all_sents = df['question'].values
    all_words = []
    for sent in all_sents:
        sent_words = sent.split(" ")
        if '?' in sent_words[-1]:
            sent_words[-1] = sent_words[-1].replace("?", "")
        all_words += sent_words
    return all_words, set(all_words)


def get_dict_most_common_ans_given_question(df):
    interesting_columns = ['question', 'answer']
    df = df[interesting_columns]
    most_common_ans_given_question = {}

    grouped = df.groupby('question')
    for question, group in grouped:
        val_counts = group['answer'].value_counts()
        most_common_ans_for_question = max(val_counts.items(), key=operator.itemgetter(1))[0]
        most_common_ans_given_question[question] = most_common_ans_for_question

    return most_common_ans_given_question


def get_instances_of_same_question_and_answer_and_qa_id(df, df_test):
    intersection_question_answer = pd.merge(df, df_test, how='inner', on=['question', 'answer', 'qa_id'])
    return len(intersection_question_answer)


def check_correlation_with_test(df_train, df_valid, df_test):
    print("Checking correlation of train with test")
    print()

    train_questions_lst = df_train['question'].values
    test_questions_lst = df_test['question'].values
    question_stats_lst = {'Train': len(train_questions_lst), 'Test': len(test_questions_lst),
                          'Intersection': len([x for x in train_questions_lst if x in test_questions_lst]),
                          'In train but not in test': len([x for x in train_questions_lst if x not in test_questions_lst]),
                          'In test but not in train': len([x for x in test_questions_lst if x not in train_questions_lst])}
    corr_series = pd.Series(question_stats_lst, name='Question stats')
    print(corr_series)
    print()

    corr_series.to_excel(os.path.join(experiment.output_dir, "Correlation of train with test.xlsx"))


    train_questions_set, test_questions_set = set(train_questions_lst), set(test_questions_lst)
    in_test_but_not_in_train = test_questions_set.difference(train_questions_set)
    question_stats_set = {'Train': len(train_questions_set), 'test': len(test_questions_set),
                          'Intersection': len(train_questions_set.intersection(test_questions_set)),
                          'In train but not in test': len(train_questions_set.difference(test_questions_set)),
                          'In test but not in train': len(test_questions_set.difference(train_questions_set))}
    corr_unique_series = pd.Series(question_stats_set, name='Unique question stats stats')
    print(corr_unique_series)
    print()

    corr_series.to_excel(os.path.join(experiment.output_dir, "Correlation of train with test - Unique.xlsx"))

    same_instances_train_test = get_instances_of_same_question_and_answer_and_qa_id(df_train, df_test)
    same_instances_valid_test = get_instances_of_same_question_and_answer_and_qa_id(df_valid, df_test)

    if same_instances_train_test > 0 or same_instances_valid_test > 0:
        print('*** Found overlaps with test')

    return in_test_but_not_in_train


def predict_with_most_common_ans_per_category(df_test):
    most_common_answer_for_each_cat = {'modality': 'no', 'organ': 'skull and contents', 'plane': 'axial', 'abnormality': 'yes'}

    df_test = df_test[df_test['category'].isin(categories_names)]
    df_test['most_common_ans'] = df_test['category'].apply(lambda cat: most_common_answer_for_each_cat[cat])

    stats_df = get_performance_stats(df_test)

    print(f"Predict with most common answer per category")
    print(stats_df)
    print()

    stats_df.to_excel(os.path.join(experiment.output_dir, "Predict with most common answer per category.xlsx"))


def get_performance_stats(df_test):
    stats_df = pd.DataFrame()
    for category in categories_names:
        df_test_cat = df_test[df_test['category'] == category]
        y_true, y_pred = list(df_test_cat['answer'].values), list(df_test_cat['most_common_ans'].values)
        acc, f1_score_val, bleu_score = fix_y_test_and_get_metrics(y_pred, y_true)
        cat_series = pd.Series({'Accuracy': acc, 'F1-Score': f1_score_val, 'BLEU': bleu_score}, name=category)
        stats_df = stats_df.append(cat_series)
    stats_df['Accuracy'] = stats_df['Accuracy'].apply(lambda x: round(x, 3))
    stats_df['BLEU'] = stats_df['BLEU'].apply(lambda x: round(x, 3))
    mean = stats_df.mean().apply(lambda x: round(x, 3))
    mean_without_abnormality = stats_df.drop(['abnormality']).mean().apply(lambda x: round(x, 3))
    stats_df = stats_df.append(pd.Series(mean, name='Average'))
    stats_df = stats_df.append(pd.Series(mean_without_abnormality, name='Average without abnormality'))
    return stats_df

def get_performance_most_common_ans_per_question_and_category(df_train_val, df_test):
    stats_df = pd.DataFrame()
    for category in categories_names:
        df_test_cat = df_test[df_test['category'] == category]
        df_train_val_cat = df_train_val[df_train_val['category'] == category]
        most_common_ans_given_question_per_cat = get_dict_most_common_ans_given_question(df_train_val_cat)
        df_test_cat['most_common_ans'] = df_test_cat['question'].apply(lambda q: most_common_ans_given_question_per_cat[q])
        y_true, y_pred = list(df_test_cat['answer'].values), list(df_test_cat['most_common_ans'].values)
        acc, f1_score_val, bleu_score = fix_y_test_and_get_metrics(y_pred, y_true)
        cat_series = pd.Series({'Accuracy': acc, 'F1-Score': f1_score_val, 'BLEU': bleu_score}, name=category)
        stats_df = stats_df.append(cat_series)
    stats_df['Accuracy'] = stats_df['Accuracy'].apply(lambda x: round(x, 3))
    stats_df['BLEU'] = stats_df['BLEU'].apply(lambda x: round(x, 3))
    mean = stats_df.mean().apply(lambda x: round(x, 3))
    mean_without_abnormality = stats_df.drop(['abnormality']).mean().apply(lambda x: round(x, 3))
    stats_df = stats_df.append(pd.Series(mean, name='Average'))
    stats_df = stats_df.append(pd.Series(mean_without_abnormality, name='Average without abnormality'))
    return stats_df


def predict_with_most_common_ans_given_question(all_df, df_test, in_test_but_not_in_train):
    train_val_df = all_df[all_df['dataset_type'].isin(['train', 'val'])]
    most_common_ans_given_question = get_dict_most_common_ans_given_question(train_val_df)

    df_test = df_test[df_test['question'].apply(lambda x: x not in in_test_but_not_in_train)]
    df_test['most_common_ans'] = df_test['question'].apply(lambda q: most_common_ans_given_question[q])

    stats_df_per_question = get_performance_stats(df_test)
    print(f"Predict with most common answer per question")
    print(stats_df_per_question)
    print()
    stats_df_per_question.to_excel(os.path.join(experiment.output_dir, "Predict with most common answer per question.xlsx"))

    test_most_common_ans_given_question = get_dict_most_common_ans_given_question(df_test)
    df_test['most_common_ans'] = df_test['question'].apply(lambda q: test_most_common_ans_given_question[q])
    stats_df_per_question = get_performance_stats(df_test)
    print(f"Predict with most common answer in test per question")
    print(stats_df_per_question)
    print()

    stats_df_per_question.to_excel(os.path.join(experiment.output_dir, "Predict with most common answer in test per question.xlsx"))

    print()

def fix_y_test_and_get_metrics(y_pred, y_true):
    y_test_fixed = fix_y_test(y_true, y_pred)
    acc = round(accuracy_score(y_test_fixed, y_pred), 2)
    f1_score_val = round(f1_score(y_test_fixed, y_pred, average='weighted'), 2)
    wrapped_y_test_fixed = [[x] for x in y_test_fixed]
    bleu_score = corpus_bleu(wrapped_y_test_fixed, y_pred)
    rounded_bleu_score = round(bleu_score, 2)
    return acc, f1_score_val, rounded_bleu_score


def get_data_dataframes_and_export_stats(to_csv=False):
    df_train, train_categorys_series = read_qa_pairs(dataset_type='train', images=False)
    df_valid, valid_categorys_series = read_qa_pairs(dataset_type='valid', images=False)
    df_test, test_categorys_series = read_qa_pairs(dataset_type='test', images=False)
    all_df = pd.concat([df_train, df_valid, df_test], axis=0, sort=False)

    print("Categories stats")
    categories_stats_df = pd.DataFrame([train_categorys_series, valid_categorys_series, test_categorys_series]).T
    sum_ser = pd.Series(categories_stats_df.sum(), name='Total')
    categories_stats_df = categories_stats_df.append(sum_ser)
    print(categories_stats_df)
    print()
    categories_stats_df.to_excel(os.path.join(experiment.output_dir, "Categories stats.xlsx"))

    if to_csv:
        df_train.to_csv(data_dir + os.sep + "train.csv", index=False)
        df_valid.to_csv(data_dir + os.sep + "valid.csv", index=False)
        df_test.to_csv(data_dir + os.sep + "test.csv", index=False)
        print(f"Wrote to_csv {data_dir}")

    return all_df, df_train, df_valid, df_test


def produce_answers_distribution_plots(all_df):
    plot_dir = '../plots' + os.sep + "answer_distribution"
    if not os.path.exists(plot_dir):
        os.mkdir(plot_dir)
    categories_names = deepcopy(list(categories_names_map.keys()))

    fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(15, 15))

    stats_df = pd.DataFrame()

    for idx, cat in enumerate(categories_names):
        df_cat = all_df[all_df['category'] == cat]
        df_cat_train = df_cat[df_cat['dataset_type'] == 'train']['answer'].value_counts()
        df_cat_valid = df_cat[df_cat['dataset_type'] == 'val']['answer'].value_counts()
        df_cat_test = df_cat[df_cat['dataset_type'] == 'test']['answer'].value_counts()

        cat_series = pd.Series({'Train': len(df_cat_train), 'Valid': len(df_cat_valid), 'Test': len(df_cat_test)}, name=cat)
        df_cat_train.plot.bar(ax=axes[0])
        df_cat_valid.plot.bar(ax=axes[1])
        df_cat_test.plot.bar(ax=axes[2])

        fig.suptitle(f"{cat} Answers distriubtion", fontsize=16)

        row_titles = ['Train', 'Valid', 'Test']
        for ax, row in zip(axes, row_titles):
            ax.set_ylabel(row, rotation=0, size='large', labelpad=40)

        plt.tight_layout()
        plt.savefig(plot_dir + os.sep + f"{cat} answers distribution" + '.png')

        stats_df = stats_df.append(cat_series)

    stats_df = stats_df[['Train', 'Valid', 'Test']]
    stats_df = stats_df.astype(int)
    print("Answer distribution")
    print(stats_df)
    print()
    stats_df.to_excel(os.path.join(experiment.output_dir, "Answer distribution.xlsx"))




def get_line_parts(line, test=False):
    if test:
        qa_id, category, question, answer = line.split("|")
        return {"qa_id": qa_id, "category": category, "question": question, "answer": answer}
    else:
        qa_id, question, answer = line.split("|")
        return {"qa_id": qa_id, "question": question, "answer": answer}


def read_qa_pairs(dataset_type, images=False):
    dir_p = dataset_type_to_dir_map[dataset_type]

    if dataset_type != 'test':
        df = pd.DataFrame()
        dir_p += os.sep + 'QAPairsByCategory'
        for cat_key, cat_value, in categories_names_map.items():
            if dataset_type == 'valid':
                dataset_type = 'val'
            cat_p = dir_p + os.sep + cat_value + "_" + dataset_type + ".txt"
            with open(cat_p, encoding='utf-8') as f:
                lines = [x.rstrip('\n') for x in f.readlines()]
            lines = [get_line_parts(line) for line in lines]
            cat_df = pd.DataFrame(lines)
            cat_df['category'] = cat_key
            df = pd.concat([df, cat_df])
            # all_categories_lines[cat] = lines
    else:
        dir_p += os.sep + 'VQAMed2019_Test_Questions_w_Ref_Answers.txt'
        with open(dir_p) as f:
            lines = [x.rstrip('\n') for x in f.readlines()]
        lines = [get_line_parts(line, test=True) for line in lines]
        df = pd.DataFrame(lines)

    # print(f"\n{dataset_type.capitalize()} dataframe ({len(df)} items)")
    category_series = pd.Series(df['category'].value_counts(), name=dataset_type.capitalize())

    df['dataset_type'] = dataset_type
    return df, category_series


if __name__ == '__main__':
    main()
    copy_tree(experiment.output_dir, data_exploration_dir)
    experiment.end()
