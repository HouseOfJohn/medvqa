import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix

from src.data_exploration import get_data_dataframes_and_export_stats
from src.utils import get_data_dataframes


def main():
    X_test, X_train_val, y_test, y_train_val = get_data()

    vectorizer = CountVectorizer()
    vectorizer.fit(X_train_val)
    X_train_val_transformed = vectorizer.transform(X_train_val)
    X_test_transformed = vectorizer.transform(X_test)
    classifier = LogisticRegression()
    classifier.fit(X_train_val_transformed, y_train_val)

    y_pred = classifier.predict(X_test_transformed)
    cm = confusion_matrix(y_test, y_pred)
    acc = accuracy_score(y_test, y_pred)
    f1_score_val = f1_score(y_test, y_pred, average='weighted')
    score_series = pd.Series({'accuracy': acc, 'weighted f1_score': f1_score_val}, name="Categories classifier scores")
    print('Confusion matrix')
    print(cm)
    print(score_series)


def get_data():
    all_df, df_train, df_valid, df_test = get_data_dataframes()
    df_train_val = all_df[all_df['dataset_type'].isin(['train', 'val'])]
    X_train_val = df_train_val['question']
    y_train_val = df_train_val['category']
    X_test = df_test['question']
    y_test = df_test['category']
    return X_test, X_train_val, y_test, y_train_val


if __name__ == '__main__':
    main()