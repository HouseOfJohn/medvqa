import os

# data_dir = r"D:\ThesisResources\Med-VQA_data" + os.sep
# data_dir = '/Users/yonatab/data/med_vqa_data' # MAC
# data_dir = '/home/jon-dl/data/med_vqa_data' # Azure-Jon-dl
data_dir = '/data/users/yonatab/med_vqa_data' # P2-Ilia

TRAIN_DIR = data_dir + os.sep + 'ImageClef-2019-VQA-Med-Training'
VALID_DIR = data_dir + os.sep + 'ImageClef-2019-VQA-Med-Validation'
TEST_DIR = data_dir + os.sep + 'ImageClef-2019-VQA-Med-Test'

training_and_val_images = data_dir + os.sep + 'training_and_val_images'

dataset_type_to_dir_map = {'train': TRAIN_DIR, 'valid': VALID_DIR, 'test': TEST_DIR}
categories_names_map = {'modality': 'C1_Modality', 'plane': 'C2_Plane', 'organ': 'C3_Organ', 'abnormality': 'C4_Abnormality'}
categories_names_without_abnormality = ['modality', 'plane', 'organ']
categories_names = ['modality', 'plane', 'organ', 'abnormality']
